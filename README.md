# Gazoduc - simulation de système de tuyauterie
Projet réalisé dans le cadre de l'UE [LIFAPCD](https://perso.liris.cnrs.fr/alexandre.meyer/public_html/www/doku.php?id=lifap4)

---

Gazoduc permet de simuler de manière ludique l'évolution d'un système de tuyauterie fermé.
Grâce à une interface intuitive, l'utilisateur peut assembler un système composé de différents éléments
(tuyaux, réservoirs, pompes, etc.), spécifier les conditions initiales, puis simuler l'évolution du système
en temps réel.

## Utilisation

### Mode graphique

Pour accéder au plein potentiel de Gazoduc, il suffit de lancer l'exécutable sans option, en spécifiant ou non l'espace de travail à ouvrir:

```bash
gazoduc
gazoduc mon_espace.gzd
```

### Mode textuel

Gazoduc peut également générer des enregistrements de simulation depuis une ligne de commande.
Pour cela, il faut nécessairement spécifier un espace de travail (`.gzd`), ainsi que l'option `-t`.

> Gazoduc sauvegarde et ouvre les fichiers `.gzd`. Cependant, il s'agit uniquement d'un fichier JSON !

```bash
gazoduc -t mon_fichier.gzd
```

Cela simulera le système et créera un fichier `mon_fichier_sortie.csv` importable dans un tableur.

Une liste exhaustive des options peut être obtenue via `gazoduc -h`.


## Compilation

### Linux
La librairie [SDL2](https://www.libsdl.org/) doit être installée, ainsi que Make.

```bash
make gazoduc
```

## Documentation
La documention peut être générée par Doxygen et ouverte directement à l'aide de la commande:

```bash
make doc
```

Il est ensuite possible d'accéder à la documentation en ouvrant le fichier `doc/html/index.html` dans un navigateur.

Il y a également quelques exemples de systèmes dans le dossier `exemples`. Ils ne sont pas exhaustifs.

Une aide est également intégrée au programme (barre de menu > `Aide ?`).

## Organisation

Le code source du programme est séparé en 3 parties distinctes:
- `external` : Les librairies externes utilisées (Dear ImGui, json, tclap)
- `sim`: Les classes nécessaires à la simulation physique
- `interface`: Les classes nécessaires à l'affichage graphique, la gestion des entrées/sorties, etc.

Il existe également un répertoire `exemples`, dans lequel se trouve plusieurs fichiers à charger
avec Gazoduc présentant des assemblages simples pour explorer les possibilités du programme.


## A propos de la simulation

Ce logiciel simule des systèmes **fermés** (bien qu'un système ouvert puisse être émulé à l'aide de réservoirs "immuables")
de tuyauterie dans lequel circule un gaz. La physique est très simplifiée (les effets adiabatiques, les frottements,
la viscosité ou encore la convection ne sont pas pris en compte), mais les résultats sont tout
de même suffisamment corrects pour avoir une idée de l'évolution du système dans le temps.

Un _système_ est composé d'**éléments** connectés entre eux. Un **élément** possède un nombre
défini de **ports** (pardonnez le langage informatique...), qui sont des interfaces auxquelles
se connectent les autres éléments. Ainsi, un système _fermé_ est un système pour lequel **tous
les éléments interconnectés ont tout leurs ports connectés**.

Une étape de simulation consiste à itérer sur chaque élément, et à calculer les gains/pertes de
matière pour chaque port de l'élément. **Un point important de la simulation est donc de s'assurer
qu'entre deux éléments, l'un perde autant de matière que l'autre n'en gagne.** Pour cela, une astuce
est utilisée (même si elle n'est fondamentalement pas nécessaire): dans un échange, seul l'élément qui
perd de la matière calcule les quantités à échanger et les applique pour les 2 éléments.

> Le code contient beaucoup d'explications concernant les équations utilisées !


## Credits
Noëllie PAULUS P2100318  
Léo JOSSERAND P2100741  

Dépot: https://forge.univ-lyon1.fr/p2100741/lifapcd-projet  
ID Projet: **27963**

Librairies utilisées:
* [SDL2](https://www.libsdl.org/)
* [Dear ImGui](https://github.com/ocornut/imgui/)
* [JSON for Modern C++](https://github.com/nlohmann/json)
* [TCLAP](https://tclap.sourceforge.net/)

Sources utilisées pour la partie simulation:
- https://www.maxicours.com/se/cours/transferts-thermiques-flux-thermique-resistance-thermique/
- https://www.tec-science.com/thermodynamics/heat/thermal-conductivity-of-gases/
- https://physics.stackexchange.com/questions/131032/calculate-flow-rate-of-air-through-a-pressurized-hole
- https://www.engineeringclicks.com/forum/threads/pressure-tank-drain-time.5515/
- https://www.quora.com/What-is-the-flow-rate-of-a-gas-leaving-a-container-at-1-ATM-into-another-that-is-in-a-vacuum
- https://fr.wikipedia.org/wiki/R%C3%A9sistance_thermique
- https://en.wikipedia.org/wiki/Specific_heat_capacity
- https://study.com/skill/learn/how-to-calculate-change-in-pressure-in-an-adiabatic-process-explanation.html
- https://www.chemteam.info/Thermochem/MixingWater.html
- https://en.wikipedia.org/wiki/Adiabatic_process