//
// Created by leo on 02/03/23.
//

#include "../../include/sim/Pompe.h"
#include <cmath>
#include <iostream>


Pompe::Pompe(long double debit) {
    if (debit < 0) {throw std::runtime_error("Le débit d'une pompe doit être >= 0");}
    this->debit = debit;
}


void Pompe::set_debit(long double dbt) {
    if (dbt < 0) {throw std::runtime_error("Le débit d'une pompe doit être >= 0");}
    this->debit = dbt;
}

long double Pompe::get_debit() const {return debit;}


long double Pompe::get_volume(unsigned int port) const {
    // Vérifie qu'un élément est bien connecté au port opposé
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'une pompe");}
    if (!this->est_connecte(!port)) {throw std::runtime_error("Accès à une pompe dont un port n'est pas connecté");}

    // appelle "get_volume" sur l'élément opposé (→ Pompe sert juste de relai entre deux éléments)
    return this->connexions.at(!port).el->get_volume(!port);
}

long double Pompe::get_pression(unsigned int port) const {
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'une pompe");}


    // On calcule une pression "fictive", qui n'est pas utilisée par la simulation
    // (la pompe effectuant le transfert d'une autre manière que les autres éléments).
    // Cette pression fictive est par exemple utilisée pour calculer le flux de matière.

    // Pour la formule de la vitesse du gaz, cf. Element::get_vitesse_gaz

    // v = √( 2(Pa - Pb) / ρ) <=> Pa = 1/2 ρ v² + Pb
    //                        <=> Pb = -1/2 ρ v² + Pa
    // avec:
    // Pa = la pression du gaz à sa source, en Pa
    // Pb = la pression du gaz à sa destination, en Pa
    // ρ = la densité du gaz à sa source, en kg m⁻³
    // v = la vitesse du flux de gaz de A vers B, en m s⁻¹


    // Port 0 = entrée, port 1 = sortie.
    // En fonction du port, la formule utilisée n'est pas la même (la pompe est soit la source, soit la destination)


    Connexion s = this->connexions.at(0);
    long double densite_source = Simulateur::get_masse_molaire(simulateur->gaz) * s.el->get_mol(s.port) / s.el->get_volume(s.port);

    if (port == 0) {
        // la pompe est la destination → on doit déterminer Pb tel que v * diametre = debit

        // si débit = 0, on renvoie la pression de la source afin qu'il n'y ait aucun transfert
        if (debit == 0) {return s.el->get_pression(s.port);}



        // détermine la vitesse via le diamètre de la connexion et le débit: vitesse = debit / pi * (diametre/2)²
        long double rayon = (s.el->get_diametre(s.port) / 2);
        long double vitesse = this->debit / (M_PI * rayon * rayon);

        // détermine la pression via vitesse et la densité
        return -1.0/2.0 * densite_source * vitesse * vitesse + s.el->get_pression(s.port);
    }
    else { // la pompe est la source → on doit déterminer Pa tel que v * diametre = debit
        Connexion dest = this->connexions.at(1);

        // si débit = 0, on renvoie la pression de la destination afin qu'il n'y ait aucun transfert
        if (debit == 0) {return dest.el->get_pression(dest.port);}

        long double rayon = (dest.el->get_diametre(dest.port) / 2);
        long double vitesse = this->debit / (M_PI * rayon * rayon);

        return 1.0/2.0 * densite_source * vitesse * vitesse + dest.el->get_pression(dest.port);
    }
}

long double Pompe::get_temp(unsigned int port) const {
    // Vérifie qu'un élément est bien connecté au port opposé
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'une pompe");}
    if (!this->est_connecte(!port)) {throw std::runtime_error("Accès à une pompe dont un port n'est pas connecté");}

    // appelle "get_temp" sur l'élément opposé (→ Pompe sert juste de relai entre deux éléments)
    return this->connexions.at(!port).el->get_temp(!port);
}

long double Pompe::get_mol(unsigned int port) const {
    // Vérifie qu'un élément est bien connecté au port opposé
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'une pompe");}
    if (!this->est_connecte(!port)) {throw std::runtime_error("Accès à une pompe dont un port n'est pas connecté");}

    // appelle "get_mol" sur l'élément opposé (→ Pompe sert juste de relai entre deux éléments)
    return this->connexions.at(!port).el->get_mol(!port);
}

long double Pompe::get_diametre(unsigned int port) const {
    Connexion c = this->connexions.at(port);        // cf get_volume
    return c.el->get_diametre(c.port);
}

unsigned int Pompe::get_nb_ports() const {return NB_PORTS;}


void Pompe::transferer_matiere(long double quantite, long double temp, unsigned int port) {
    // Rien à faire
}



void Pompe::transfert_manuel(long double delta_t) const {
    Connexion source = this->connexions.at(0);
    Connexion dest = this->connexions.at(1);

    // Le volume à transférer. Il est majorée par le volume de la source
    long double volume_transfert = debit * delta_t;
    if (volume_transfert > source.el->get_volume(source.port)) {volume_transfert = source.el->get_volume(source.port);}

    // On détermine la quantité de matière provenant de la source
    long double densite_source = Simulateur::get_masse_molaire(simulateur->gaz) * source.el->get_mol(source.port) / source.el->get_volume(source.port);
    long double quantite_transfert = densite_source * volume_transfert;

    long double temp_transfert = source.el->get_temp(source.port);

    // on transfère la matière
    source.el->transferer_matiere(-quantite_transfert, temp_transfert, source.port);
    dest.el->transferer_matiere(quantite_transfert, temp_transfert, dest.port);
}



inline void Pompe::update_nom() {
    this->nom = "Pompe #" + std::to_string(this->id_element);
}

inline unsigned int Pompe::get_id_type() const {
    return ID_TYPE;
}
