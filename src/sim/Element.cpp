//
// Created by leo on 28/02/23.
//

#include "../../include/sim/Element.h"
#include "../../include/sim/Constantes.h"
#include "../../include/sim/EchangeurThermique.h"
#include "../../include/sim/Pompe.h"

#include <cmath>
#include <iostream>










Element::~Element() = default;



void Element::update(long double delta_t) {
    // Effectue la mise à jour sur tous les ports de l'Element
    for (unsigned int p=0; p<this->get_nb_ports(); p++) {


        // Si on est un élément nécessitant une update particulière, on l'applique ici avant toute chose
        if (get_id_type() == 11) {
            auto* ptr = (EchangeurThermique*) this;
            ptr->echange_thermique(delta_t);
        }
        if (get_id_type() == 13) {
            auto* ptr = (Pompe*) this;
            ptr->transfert_manuel(delta_t);
        }


        Connexion c = this->connexions.at(p);


        // Si ce transfert implique une pompe, on ne fait rien: la pompe
        // effectue le transfert de la source à la destination "manuellement" via sa fonction
        // transfert_manuel()
        if (c.el->get_id_type() == 13 || this->get_id_type() == 13) {continue;}


        // Calcul le diamètre de l'orifice, c.-à-d. le diamètre minimum entre les deux éléments
        long double diam = std::min(this->get_diametre(p), c.el->get_diametre(c.port));

        // vitesse du gaz entre les deux éléments
        long double vitesse = this->get_vitesse_gaz(p);

        // calcul le volume de gaz déplacé: m s⁻¹ * s * m² = m³
        long double volume = vitesse * delta_t * diam;

        // Pour faciliter les calculs, on identifie les éléments en fonction de celui qui perd de la matière
        // et celui qui en gagne.
        Connexion perdant, gagnant;
        bool est_perdant = false;
        Connexion c_self {this, p}; // connexion à soit même pour simplifier le code suivant
        if (volume < 0.0) {gagnant = c_self; perdant = c;}
        else if (volume > 0.0) {perdant = c_self; gagnant = c; est_perdant = true;}
        else {continue;} // aucun transfert à effectuer, car volume = 0.0


        // seul le perdant calcul la quantité de matière transférée et applique ce transfert
        if (est_perdant) {
            // Calcul de la quantité de matière à transférer pour que les pressions s'équilibrent
            // on assigne des variables pour raccourcir la taille de l'équation
            long double tp = perdant.el->get_temp(perdant.port);
            long double tg = gagnant.el->get_temp(gagnant.port);
            long double np = perdant.el->get_mol(perdant.port);
            long double ng = gagnant.el->get_mol(gagnant.port);
            long double vp = perdant.el->get_volume(perdant.port);
            long double vg = gagnant.el->get_volume(gagnant.port);

            long double max_transfere = (tp * np * vg - tg * ng * vp) / (tp * (vp + vg));


            // échange entre le perdant et le gagnant sans considérer la limite
            long double diff = perdant.el->get_mol(perdant.port) / perdant.el->get_volume(perdant.port) * volume;

            // On prend le minimum entre diff et quant_transferee_max
            if (diff > max_transfere) {diff = max_transfere;}


            perdant.el->transferer_matiere(-diff, perdant.el->get_temp(perdant.port), perdant.port);
            gagnant.el->transferer_matiere(diff, perdant.el->get_temp(perdant.port), gagnant.port);
        }
    }
}



long double Element::get_vitesse_gaz(unsigned int port) const {
    // Pa + 1/2 ρ v² = Pb <=> v = √( 2(Pb - Pa) / ρ )
    // On change le signe de (Pb - Pa) pour obtenir la vitesse de A vers B (sinon, v est imaginaire)
    // v = √( 2(Pa - Pb) / ρ )
    // avec:
    // Pa = pression de l'élément A, en Pa
    // Pb = pression de l'élément B, en Pa
    // ρ = la densité du gaz dans l'élément A, en kg m⁻³
    // v = la vitesse du flux de gaz de A vers B, en m s⁻¹

    Connexion connexion = this->connexions.at(port);

    // Si un volume de 0 est renvoyé, il y a un problème quelque part...
    if (this->get_volume(port) <= 0) {throw std::runtime_error("Existence d'un élément de volume = 0m³");}

    long double pression_a = this->get_pression(port);
    long double pression_b = connexion.el->get_pression(connexion.port);

    // Il faut que pression_a > pression_b. Si ce n'est pas le cas, on appelle get_vitesse_gaz
    // sur l'autre élément, et on inverse simplement le signe de la vitesse!
    if (pression_a < pression_b) {
        return -connexion.el->get_vitesse_gaz(connexion.port);
    }

    // En dessous d'un certain seuil de précision, on arrondit à 0 la vitesse
    // pour que l'état de la simulation se stabilise.
    if (pression_a - pression_b < 1) {return 0;}

    // Calcul de la densité du gaz pour A.
    long double densite = Simulateur::get_masse_molaire(simulateur->gaz) * this->get_mol(port) / this->get_volume(port);

    // la vitesse maximum du gaz est Mach 1.
    if (densite == 0) {
        return MACH_1;
    }

    long double vitesse = sqrt(2 * (pression_a - pression_b) / densite); // forcément > 0
    if (vitesse > MACH_1) {vitesse = MACH_1;}


    return vitesse;
}



void Element::set_simulateur(Simulateur* sim) {this->simulateur = sim;}
void Element::set_id(unsigned int id) {
    this->id_element = id;
    this->update_nom();
}

void Element::set_connexion(unsigned int port, Element* autre_el, unsigned int autre_port) {
    // Vérifie qu'un élément n'est pas connecté sur le même port
    if (this->est_connecte(port)) {
        throw std::runtime_error("Tentative de connexion de deux éléments sur le même port");
    }

    Connexion c;
    c.port = autre_port;
    c.el = autre_el;
    this->connexions.insert(std::make_pair(port, c));
}


bool Element::est_connecte(unsigned int port) const {
    return this->connexions.find(port) != this->connexions.end();
}


