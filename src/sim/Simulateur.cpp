//
// Created by leo on 28/02/23.
//

#include "../../include/sim/Simulateur.h"
#include "../../include/sim/Constantes.h"
#include "../../include/sim/EchangeurThermique.h"

#include <cstdlib>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iostream>


Simulateur::Simulateur(Gaz gaz) {
    this->gaz = gaz;
}

Simulateur::~Simulateur() {
    // désalloue tous les éléments
    for (auto el: this->elements) {
        delete el.second;
    }
}


long double Simulateur::get_ctm(Gaz gaz) {
    switch (gaz) {
        case HELIUM: return 5193.1;
        case DIHYDROGENE: return 14300;
        case DIOXYGENE: return 919;
        //case VAPEUR_EAU: return 2010;
        default: throw std::runtime_error("Gaz inconnu");
    }
}


long double Simulateur::get_ct(Materiau mat) {
    switch (mat) {
        case CUIVRE: return 400;
        case ACIER: return 47;
        case VERRE: return 0.498;
        default: throw std::runtime_error("Matériau inconnu");
    }
}


long double Simulateur::get_masse_molaire(Gaz gaz) {
    switch (gaz) {
        case HELIUM: return 4.002602E-3;
        case DIHYDROGENE: return 2.016E-3;
        case DIOXYGENE: return 3.1998E-2;
        //case VAPEUR_EAU: return 1.8015E-2;
        default: throw std::runtime_error("Gaz inconnu");
    }
}

long double Simulateur::get_masse_volumique(Materiau mat) {
    switch (mat) {
        case CUIVRE: return 8960;
        case ACIER: return 7859;
        case VERRE: return 2520;
        default: throw std::runtime_error("Matériau inconnu");
    }
}



long double Simulateur::get_capa_thermique(Materiau mat) {
    switch (mat) {
        case CUIVRE: return 384.4;
        case ACIER: return 475;
        case VERRE: return 750;
        default: throw std::runtime_error("Matériau inconnu");
    }
}


long double Simulateur::get_masse_moleculaire(Gaz gaz) {
    switch (gaz) {
        case HELIUM: return 6.646477E-27;
        case DIHYDROGENE: return 3.348E-27;
        case DIOXYGENE: return 5.3134E-26;
        //case VAPEUR_EAU: return 2.9915E-26;
        default: throw std::runtime_error("Gaz inconnu");
    }
}


long double Simulateur::get_diametre(Gaz gaz) {
    switch (gaz) {
        case HELIUM: return 6.2E-11;
        case DIHYDROGENE: return 7.414E-11;
        case DIOXYGENE: return 1.2E-10;
        //case VAPEUR_EAU: return 2.75E-10;
        default: throw std::runtime_error("Gaz inconnu");
    }
}


long double Simulateur::get_indice_addia(Gaz gaz) {
    switch (gaz) {
        case HELIUM: return ID_ADDIA_MONO;
        case DIHYDROGENE: return ID_ADDIA_DI;
        case DIOXYGENE: return ID_ADDIA_DI;
        //case VAPEUR_EAU: return ???;
        default: throw std::runtime_error("Gaz inconnu");
    }
}


Element* Simulateur::ajouter_element(Element* el, unsigned int id) {
    // vérifie que le nombre max d'éléments n'a pas été atteint
    unsigned int id_el_max = pow(10, NB_CHIFFRES_ID);

    if (this->elements.size() == id_el_max) {
         throw std::runtime_error("Limite d'éléments atteinte");
    }

    // génère un id unique si un id n'a pas été fourni
    if (id == 0) {
        unsigned int id_type = el->get_id_type();
        do {id = (rand() % id_el_max) + (id_type * id_el_max);}    // les deux premiers nombres de l'identifiant donnent son type
        while (this->elements.find(id) != this->elements.end());
    } else {
        // S'assure que l'identifiant fournit n'a pas déjà été ajouté
        if (this->elements.find(id) != this->elements.end()) {
             throw std::runtime_error("Tentative de création de deux éléments avec le même identifiant");
        }
    }

    el->set_simulateur(this);
    el->set_id(id);

    this->elements.insert(std::make_pair(id, el));
    return el;
}


void Simulateur::lier_elements(unsigned int id_el_1, unsigned int port_el_1, unsigned int id_el_2,
                               unsigned int port_el_2) {
    // Vérifie que les éléments existent
    if (this->elements.find(id_el_1) == this->elements.end()) {throw std::runtime_error("Tentative de lien d'éléments inexistants");}
    if (this->elements.find(id_el_2) == this->elements.end()) {throw std::runtime_error("Tentative de lien d'éléments inexistants");}

    Element* el1 = this->elements.at(id_el_1);
    Element* el2 = this->elements.at(id_el_2);

    // Vérifie que les éléments n'ont pas de connexions au port spécifié
    if (el1->est_connecte(port_el_1)) {throw std::runtime_error("Tentative de lien d'un élément sur un port déjà connecté");}
    if (el2->est_connecte(port_el_2)) {throw std::runtime_error("Tentative de lien d'un élément sur un port déjà connecté");}

    // Connecte les éléments entre eux
    el1->set_connexion(port_el_1, el2, port_el_2);
    el2->set_connexion(port_el_2, el1, port_el_1);
}


void Simulateur::lier_elements(Element* ptr_el_1, unsigned int port_el_1, Element* ptr_el_2,
                               unsigned int port_el_2) {

    // Vérifie que les éléments n'ont pas de connexions au port spécifié
    if (ptr_el_1->est_connecte(port_el_1)) {throw std::runtime_error("Tentative de lien d'un élément sur un port déjà connecté");}
    if (ptr_el_2->est_connecte(port_el_2)) {throw std::runtime_error("Tentative de lien d'un élément sur un port déjà connecté");}

    // Connecte les éléments entre eux
    ptr_el_1->set_connexion(port_el_1, ptr_el_2, port_el_2);
    ptr_el_2->set_connexion(port_el_2, ptr_el_1, port_el_1);
}




Element& Simulateur::get_element(unsigned int id) {
    // Vérifie que l'élément existe
    if (this->elements.find(id) == this->elements.end()) {throw std::runtime_error("Accès à un élément inexistant");}

    return *this->elements.at(id);
}



void Simulateur::simuler_etape(long double delta_t) {

    if (delta_t > MAX_STEP) {
        while (delta_t >= MAX_STEP) {
            simuler_etape(MAX_STEP);
            delta_t -= MAX_STEP;
        }
    }

    if (delta_t > 0) {
        // met à jour chaque élément
        for (auto& el: this->elements) {
            el.second->update(delta_t);
        }
    }
}





void Simulateur::generer_csv(const std::string& nom_fichier, long double duree, long double interval, bool ecrase) {
    if (interval <= 0.0) {throw std::runtime_error("L'intervalle de simulation doit être > 0");}
    if (duree < interval) {throw std::runtime_error("La durée de simulation ne peut pas être inférieure à un intervalle de simulation");}

    // Si ecrase est false, on ne peut générer le fichier que si nom_fichier n'existe pas encore
    if (!ecrase) {
        std::ifstream test;
        test.open(nom_fichier);
        if (!test.fail()) {
            test.close();
            throw std::runtime_error("Un fichier du même nom existe déjà (" + nom_fichier + ")");
        }
        test.close();
    }

    const char SEPARATEUR = ',';

    // on ne mettra dans le fichier que les données issues des réservoirs et des échangeurs thermiques.
    const unsigned int A_ENREGISTRER[3] = {10, 11, 14};


    // récupère les éléments enregistrables dans un ordre fixe (nécessaire pour le CSV)
    std::vector<unsigned int> id_a_enregistrer;
    for (auto el: this->elements) {
        unsigned int id_type = el.first / pow(10, NB_CHIFFRES_ID);

        // vérifie que l'élément doit être enregistré ou non.
        bool a_enregistrer = false;
        for (auto id: A_ENREGISTRER) {
            if (id == id_type) {a_enregistrer = true; break;}
        }

        if (a_enregistrer) {id_a_enregistrer.push_back(el.first);}
    }

    // On trie les éléments selon leur id. Comme les premiers chiffres de l'id correspondent à leur type,
    // cela équivaut à les regrouper en fonction de leur type (Tuyau, Réservoirs etc.)
    // de plus, les id de types sont assignés de telle sorte que les éléments les + importants ont l'id le plus bas
    // (ex: les réservoirs ont le plus petit id, 10, car ils sont les éléments les + intéressants)
    std::sort(id_a_enregistrer.begin(), id_a_enregistrer.end());

    std::ofstream sortie;
    fixed(sortie);          // les nombres seront toujours affichés en notation décimale avec max 4 chiffres décimaux
    sortie.precision(4);
    sortie.open(nom_fichier, std::ios::out);
    if (sortie.fail()) {
        throw std::runtime_error("Impossible d'ouvrir le fichier (" + nom_fichier + ")");
    }


    // On peut maintenant écrire dans le fichier. On commence par le nom des colonnes.
    sortie << "Temps écoulé (s)";
    for (auto id_el: id_a_enregistrer) {
        sortie << SEPARATEUR;

        Element& el = this->get_element(id_el);

        // Valeurs communes à tous les contenants
        sortie << "Quantité matière " << el.nom << " (mol)" << SEPARATEUR;
        sortie << "Pression " << el.nom << " (Pa)" << SEPARATEUR;
        sortie << "Température " << el.nom << " (K)";

        // Si on parle d'un échangeur thermique, on ajoute d'autres valeurs
        if (id_el / pow(10, NB_CHIFFRES_ID) == 11) {
            sortie << SEPARATEUR << "Extérieur " << el.nom << " (K)" << SEPARATEUR;
            sortie << "Flux thermique " << el.nom << " (W)";
        }
    }
    sortie << std::endl << std::flush;



    // Boucle de simulation
    long double temps_ecoule = 0.0;
    while (temps_ecoule < duree) {
        // on convertit le temps écoulé en secondes
        sortie << temps_ecoule;

        for (auto id_el: id_a_enregistrer) {
            sortie << SEPARATEUR;

            Element& el = this->get_element(id_el);

            // Valeurs communes à tous les contenants
            sortie << el.get_mol(0) << SEPARATEUR;
            sortie << el.get_pression(0) << SEPARATEUR;
            sortie << el.get_temp(0);

            // Si on parle d'un échangeur thermique, on ajoute d'autres valeurs
            if (id_el / pow(10, NB_CHIFFRES_ID) == 11) {
                auto el_et = (EchangeurThermique&) el; // valable grâce au test précédent
                sortie << SEPARATEUR << el_et.get_temp_espace() << SEPARATEUR;
                sortie << el_et.get_ft_gaz_metal();
            }
        }
        sortie << std::endl << std::flush;
        this->simuler_etape(interval);

        temps_ecoule += interval;
    }


    // ferme le fichier
    sortie.close();
}