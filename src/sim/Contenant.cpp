//
// Created by leo on 02/03/23.
//

#include "../../include/sim/Constantes.h"
#include "../../include/sim/Contenant.h"

long double Contenant::get_volume(unsigned int port) const {return this->volume;}

long double Contenant::get_temp(unsigned int port) const {return this->temperature;}

long double Contenant::get_mol(unsigned int port) const {return this->quantite_mat;}

long double Contenant::get_pression(unsigned int port) const {
    return (this->quantite_mat * R * this->temperature) / (this->volume);
}


long double Contenant::get_diametre(unsigned int port) const {
    return this->diametre;
}



long double Contenant::get_nouvelle_temp(long double cq, long double new_temp) const {
    // Dans le cas où on obtiendrait un vide parfait, on fixe la température à 0 K.
    // Cependant, c'est une valeur arbitraire, car il est absurde de parler de température du vide.
    if (this->quantite_mat + cq <= 0) {
        return 0;
    }

    // Si les deux températures sont égales, il n'y a pas de changement de température
    if (this->temperature == new_temp) {return new_temp;}

    // Calcule de la nouvelle température du gaz après l'ajout de matière à une autre température,
    // or effet adiabatique (baisse/hausse de la température à cause de la pression)
    long double temp_froide, temp_chaude;
    long double quant_froide, quant_chaude;
    if (this->temperature < new_temp) {
        temp_froide = this->temperature;
        quant_froide = this->quantite_mat;
        temp_chaude = new_temp;
        quant_chaude = cq;
    }
    else {
        temp_chaude = this->temperature;
        quant_chaude = this->quantite_mat;
        temp_froide = new_temp;
        quant_froide = cq;
    }






    long double moy_temp = (quant_chaude * temp_chaude + quant_froide * temp_froide) / (quant_chaude + quant_froide);


    return moy_temp;


    // NE MARCHE PAS, TROP DUR, A L'AIDE
    /*

    // processus adiabatique (évolution de la quantité de matière):
    // A: Pi^(1-γ) Ti^γ = Pf^(1-γ) Tf^γ  (pour le même volume)
    //
    // avec:
    // Pi, Ti: pressions et températures initiales
    // Pf, Tf: pressions et températures finales
    // γ: l'indice adiabatique du gaz
    //
    // avec la formule des gaz parfaits P = (m/M)RT, on peut calculer la nouvelle pression à partir des conditions
    // initiales, puis déterminer la nouvelle température à l'aide de la formule A

    long double id_addia = Simulateur::get_indice_addia(simulateur->gaz);

    long double cte_addia;
    if (this->get_pression(0) == 0.0) {cte_addia = pow(moy_temp, id_addia);    }
    else {cte_addia = pow(this->get_pression(0), 1 - id_addia) * pow(moy_temp, id_addia);}

    // calcul de la nouvelle pression
    long double nouvelle_p = (this->quantite_mat + cq) * R * moy_temp / this->volume;

    // renvoie de la nouvelle température
    return pow(cte_addia / pow(nouvelle_p, 1 - id_addia), 1/id_addia);

     */
}