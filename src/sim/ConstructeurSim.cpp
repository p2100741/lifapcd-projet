//
// Created by leo on 15/03/23.
//

#include "../../include/sim/ConstructeurSim.h"
#include "../../include/sim/Reservoir.h"
#include "../../include/interface/ReservoirGraphique.h"
#include "../../include/interface/ValveGraphique.h"
#include "../../include/sim/Valve.h"
#include "../../include/interface/TuyauGraphique.h"
#include "../../include/sim/Tuyau.h"
#include "../../include/interface/PompeGraphique.h"
#include "../../include/sim/EchangeurThermique.h"
#include "../../include/interface/EchangeurThmGraphique.h"


#include <cassert>
#include <stdexcept>
#include <algorithm>


/**
 * @brief Calcul les coordonnées et la rotation globale d'un port
 * @param el L'élément auquel appartient le port
 * @param id_port L'identifiant du port sur l'élément (0, 1, etc.)
 * @param coo_port la coordonnée relative du port (par rapport à la coordonnée de l'élément)
 * @param rota_port  la rotation relative du port (par rapport à la rotation de l'élément)
 * @return Les informations absolue (par rapport à la grille) du port
 */
InfoPort get_port_coorota(ElementGraphique* el, unsigned int id_port, Couple coo_port, unsigned int rota_port) {
    // rotation absolue = (rotation_el + rotation_port) % 4
    unsigned int rota_absolue = (rota_port + el->rota) % 4;

    Couple coo_apres_rota = coo_port.rotation(el->rota);

    // Coo absolues
    InfoPort coo_abs(el, id_port, Couple(coo_apres_rota.x + el->pos.x, coo_apres_rota.y + el->pos.y), rota_absolue);
    coo_abs.rota = rota_absolue;

    return coo_abs;
}


/**
 * @brief Renvoie les informations que doit avoir un port pour se connecter au port spécifié
 * @param port les informations du port
 * @return les informations que doit avoir un port pour se connecter au port spécifié
 */
InfoPort get_info_connecte(InfoPort p) {
     Couple coo = Couple(0,0);
     unsigned int rota;

     switch (p.rota) {
         case 0: {
             coo.x = p.coo.x;
             coo.y = p.coo.y + 1;
             rota = 2;
             break;
         }
         case 1: {
             coo.x = p.coo.x + 1;
             coo.y = p.coo.y;
             rota = 3;
             break;
         }
         case 2: {
             coo.x = p.coo.x;
             coo.y = p.coo.y - 1;
             rota = 0;
             break;
         }
         case 3: {
             coo.x = p.coo.x - 1;
             coo.y = p.coo.y;
             rota = 1;
             break;
         }
         default: assert(0);
     }

     // nullprt & 0 -> valeurs quelconques
     return InfoPort(nullptr, 0, coo, rota);
}







/**
 * @brief Supprime dans element_fermes tous les éléments qui sont ouverts ou connectés à un élément ouvert
 * @param el le pointeur vers l'élément à vérifier
 * @param elements la map contenant tous les éléments graphiques
 * @param liens la liste de liens entre éléments
 * @param element_fermes la liste finale dans laquelle supprimer les éléments non-valides
 */
void supprimer_connectes(ElementGraphique* el, std::vector<ConnexionEspaceTravail>& liens, std::unordered_map<ElementGraphique*, unsigned int>& elements, std::vector<ElementGraphique*>& el_fermes) {
        // Supprime l'élément dans la liste des éléments, s'il se trouve dedans
    auto pos = std::find(el_fermes.begin(), el_fermes.end(), el);
    if (pos != el_fermes.end()) {
        el_fermes.erase(pos);
    }

    if (!liens.empty()) {
        for (long unsigned int i=0; i<liens.size(); i++) {
            ConnexionEspaceTravail l = liens[i];

            if (l.el1->id == el->id) {
                liens.erase(liens.begin() + i); // supprime le lien
                supprimer_connectes(l.el2, liens, elements, el_fermes);
            }
            else if (l.el2->id == el->id) {
                liens.erase(liens.begin() + i); // supprime le lien
                supprimer_connectes(l.el1, liens, elements, el_fermes);
            }
        }
    }
}







Simulateur* construire_simulateur(EspaceTravail& espace_travail) {
    Gaz gaz;
    switch (espace_travail.gaz) {
        case 0: gaz = HELIUM; break;
        case 1: gaz = DIHYDROGENE; break;
        case 2: gaz = DIOXYGENE; break;
        default: throw std::runtime_error("Gaz de l'espace de travail inconnu");
    };

    auto* sim = new Simulateur(gaz);


    // on récupère tous les éléments de l'espace de travail ainsi que les informations de leurs ports
    std::unordered_map<ElementGraphique*, unsigned int> elements_nb_cx; // Couple élément - Nombre de connexions
    std::vector<InfoPort> info_ports;
    for (auto el: espace_travail.element) {
        ElementGraphique* element = el.second;
        elements_nb_cx.insert(std::make_pair(element, 0));

        std::vector<Couple> coo_ports = element->get_coo_ports();
        std::vector<unsigned int> rota_ports = element->get_orientation_ports();

        for (long unsigned int i=0; i<coo_ports.size(); i++) {
            info_ports.push_back(get_port_coorota(element, i, coo_ports[i], rota_ports[i]));
        }
    }




    // Calcul les liens entre les ports
    std::vector<ConnexionEspaceTravail> liens;
    for (auto p: info_ports) {
        // Pour chaque port, on cherche un port auquel il est connecté. On ajoute ce lien dans le vector
        // (pour éviter les doublons (A, B) (B, A), on s'assure que A < B).

        // information que doit avoir un port pour être connecté
        InfoPort info_connexion = get_info_connecte(p);

        // On cherche un UNIQUE port connecté à p tq p.el < op.el. Si on en trouve un deuxième,
        // cela veut dire que 2 ports sont superposés l'un sur l'autre, et donc que l'EspaceTravail n'est pas valide
        bool connecte = false;
        for (auto op: info_ports) {
            if (op.coo == info_connexion.coo && op.rota == info_connexion.rota && p.el < op.el) {
                if (connecte) {throw std::runtime_error("Deux ports sont superposés");}

                // On ajoute la connexion à l'élément. Cela permettra de déterminer si l'élément manque une connexion ou non
                elements_nb_cx.at(p.el) += 1;
                elements_nb_cx.at(op.el) += 1;
                liens.emplace_back(p.el, op.el, p.id_port, op.id_port);
                connecte = true;
            }
        }
    }


    // Maintenant, pour chaque élément, on vérifie que son nombre de connexions correspond à son nombre de ports.
    // Si ce n'est pas le cas, l'élément n'est pas simulable ainsi que tous les éléments auquel il est connecté.
    // On récupère de cette manière une liste d'éléments qui constituent tous un système fermé

    std::vector<ElementGraphique*> el_fermes;
    for (auto& el: elements_nb_cx) {
        el_fermes.push_back(el.first);
    }
    // On enlève petit à petit les éléments dans el_fermes.
    for (auto& el: elements_nb_cx) {
        if (el.first->get_coo_ports().size() != el.second) {
            supprimer_connectes(el.first, liens, elements_nb_cx, el_fermes);
        }
    }

    // S'il n'y a pas d'éléments simulables, on renvoie une erreur.
    if (el_fermes.empty()) {
        throw std::runtime_error("Aucun élément à simuler. Vérifiez que votre système est bien fermé.");
    }

    // Enfin, on a une liste d'éléments appartenant à un système fermé et une liste de liens entre eux.
    // On peut donc construire la simulation
    for (auto el: el_fermes) {

        switch (el->get_id_type()) {

            // Réservoir
            case 10: {
                auto* ptr = (ReservoirGraphique*) el; // on cast le pointeur pour accéder à ses données
                Element* el_ptr = sim->ajouter_element(new Reservoir(ptr->volume, ptr->temp, ptr->pression, ptr->diametre, ptr->immuable), ptr->id);
                // on lie l'ElementGraphique à son Element correspondant
                ptr->elt_correspd = el_ptr;

                break;
            }


            // Echangeur Thermique
            case 11: {
                auto* ptr = (EchangeurThmGraphique*) el; // on cast le pointeur pour accéder à ses données
                Espace esp = {ptr->volume_espace, ptr->temperature_espace, ptr->espace_immuable};
                Element* el_ptr = sim->ajouter_element(new EchangeurThermique(esp, ptr->diametre, ptr->longueur, ptr->epaisseur, ptr->temperature, ptr->pression, ptr->materiau), ptr->id);
                // on lie l'ElementGraphique à son Element correspondant
                ptr->elt_correspd = el_ptr;

                break;
            }


            // Valve
            case 12: {
                auto* ptr = (ValveGraphique*) el;
                Element* el_ptr = sim->ajouter_element(new Valve(ptr->ouverture), ptr->id);
                // on lie l'ElementGraphique à son Element correspondant
                ptr->elt_correspd = el_ptr;

                break;
            }

            // Pompe
            case 13: {
                auto* ptr = (PompeGraphique*) el;
                Element* el_ptr = sim->ajouter_element(new Pompe(ptr->debit), ptr->id);
                // on lie l'ElementGraphique à son Element correspondant
                ptr->elt_correspd = el_ptr;

                break;
            }


            // Les différents TuyauGraphique ont leurs id dans les 20aines, mais on les passe à 14 pour leur équivalent
            // dans la simulation (qui sont tous les mêmes).

            // Tuyau 1x3
            case 20: {
                auto* ptr = (TuyauGraphique*) el;
                Element* el_ptr = sim->ajouter_element(new Tuyau(ptr->diametre, ptr->longueur, ptr->pression, ptr->temp), ptr->id);
                // on lie l'ElementGraphique à son Element correspondant
                ptr->elt_correspd = el_ptr;

                break;
            }

            // Tuyau 1x5
            case 21: {
                auto* ptr = (TuyauGraphique*) el;
                Element* el_ptr = sim->ajouter_element(new Tuyau(ptr->diametre, ptr->longueur, ptr->pression, ptr->temp), ptr->id);
                // on lie l'ElementGraphique à son Element correspondant
                ptr->elt_correspd = el_ptr;

                break;
            }


            // Tuyau 2x2
            case 22: {
                auto* ptr = (TuyauGraphique*) el;
                Element* el_ptr = sim->ajouter_element(new Tuyau(ptr->diametre, ptr->longueur, ptr->pression, ptr->temp), ptr->id);
                // on lie l'ElementGraphique à son Element correspondant
                ptr->elt_correspd = el_ptr;

                break;
            }


            // Tuyau 4x4
            case 23: {
                auto* ptr = (TuyauGraphique*) el;
                Element* el_ptr = sim->ajouter_element(new Tuyau(ptr->diametre, ptr->longueur, ptr->pression, ptr->temp), ptr->id);
                // on lie l'ElementGraphique à son Element correspondant
                ptr->elt_correspd = el_ptr;

                break;
            }


            // Tuyau 1x1
            case 24: {
                auto* ptr = (TuyauGraphique*) el;
                Element* el_ptr = sim->ajouter_element(new Tuyau(ptr->diametre, ptr->longueur, ptr->pression, ptr->temp), ptr->id);
                // on lie l'ElementGraphique à son Element correspondant
                ptr->elt_correspd = el_ptr;

                break;
            }


            default: {throw std::runtime_error("Element inconnu dans l'espace de travail (" + std::to_string(el->get_id_type()) + ")");}

        }

    }

    // On lit les éléments dans la simulation
    for (auto l: liens) {
        sim->lier_elements(l.el1->elt_correspd, l.port_el_1, l.el2->elt_correspd, l.port_el_2);
    }

    return sim;
}



