//
// Created by leo on 02/03/23.
//

#include "../../include/sim/Reservoir.h"
#include "../../include/sim/Constantes.h"

#include <iostream>


Reservoir::Reservoir(long double volume, long double temp, long double pression, long double diametre, bool immuable) {
    if (volume <= 0) {throw std::runtime_error("Le volume d'un réservoir doit être > 0");}
    if (temp <= 0) {throw std::runtime_error("La température d'un réservoir doit être > 0");}
    if (pression < 0) {throw std::runtime_error("La pression d'un réservoir doit être >= 0");}

    // donnée membre de Reservoir
    this->immuable = immuable;

    // données membres issues de Contenant
    this->volume = volume;
    this->temperature = temp;
    this->diametre = diametre;
    this->quantite_mat = (volume * pression) / (R * temp); // PV = nRT

    // données membres issues de Element
    this->simulateur = nullptr;
    this->id_element = 0;
}

unsigned int Reservoir::get_nb_ports() const {return NB_PORTS;}


void Reservoir::transferer_matiere(long double quantite, long double temp, unsigned int port) {
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'un réservoir");}

    // ignore le transfert en cas d'immuabilité
    if (this->immuable) {return;}

    // mise à jour des valeurs membres
    this->quantite_mat += quantite;
    this->temperature = get_nouvelle_temp(quantite, temp);
}


inline void Reservoir::update_nom() {
    this->nom = "Réservoir #" + std::to_string(this->id_element);
}

inline unsigned int Reservoir::get_id_type() const {
    return ID_TYPE;
}