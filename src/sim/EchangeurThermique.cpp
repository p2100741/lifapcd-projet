//
// Created by leo on 02/03/23.
//

#include "../../include/sim/EchangeurThermique.h"
#include "../../include/sim/Constantes.h"

#include <cmath>
#include <string>
#include <stdexcept>


EchangeurThermique::EchangeurThermique(Espace espace, long double diametre, long double longueur, long double epaisseur,
                                       long double temperature, long double pression, Materiau materiau) {
    // empêche un volume nul et une épaisseur nulle
    if (diametre <= 0.0) {throw std::runtime_error("Le diamètre d'un échangeur thermique doit être > 0");}
    if (longueur <= 0.0) {throw std::runtime_error("La longueur d'un échangeur thermique doit être > 0");}
    if (epaisseur <= 0.0) {throw std::runtime_error("L'épaisseur d'un échangeur thermique doit être > 0");}

    long double rayon = diametre / 2.0;

    // données membres propres à EchangeurThermique
    this->espace = espace;
    this->materiau = materiau;
    this->epaisseur = epaisseur;
    this->surface_interne = diametre * M_PI * longueur;
    this->surface_externe = (2 * epaisseur + diametre) * M_PI * longueur;
    this->temp_metal = espace.temperature;
    this->volume_tuyau = ((rayon + epaisseur) * (rayon + epaisseur) - (rayon * rayon)) * longueur * M_PI;


    // données membres issues de Contenant
    this->volume = M_PI * rayon * rayon * longueur;
    this->diametre = diametre;
    this->temperature = temperature;
    this->quantite_mat = (pression * this->volume) / (this->temperature * R);

    // données membres issues de Element
    this->simulateur = nullptr;
    this->id_element = 0;
    this->update_nom();
}


unsigned int EchangeurThermique::get_nb_ports() const {return NB_PORTS;}


void EchangeurThermique::transferer_matiere(long double quantite, long double temp, unsigned int port) {
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'un échangeur thermique");}

    // mise à jour des valeurs membres
    this->quantite_mat += quantite;
    this->temperature = get_nouvelle_temp(quantite, temp);
}





void EchangeurThermique::echange_thermique(long double delta_t) {
    // deux échanges ont lieu: entre le gaz et les tuyaux, et entre les tuyaux et l'espace extérieur


    long double capa_thermique_tuyau = Simulateur::get_capa_thermique(materiau) * get_masse_tuyau();
    long double capa_thermique_gaz = Simulateur::get_ctm(simulateur->gaz) * Simulateur::get_masse_molaire(simulateur->gaz) * quantite_mat;
    long double capa_thermique_espace = CTV_AIR * espace.volume;



    // ## échange gaz <-> tuyau
    long double delta_gaz_tuyau = temperature - temp_metal;
    if (delta_gaz_tuyau != 0) {
        long double chaleur = get_ft_gaz_metal() * delta_t;
        if (chaleur < 0) {chaleur = -chaleur;}

        // On calcule la chaleur à transférer pour que les températures s'égalisent.
        // On ne doit pas transférer plus que cette chaleur à chaque étape de simulation,
        // afin d'éviter que la simulation s'emballe.
        long double chaleur_max = (delta_gaz_tuyau * capa_thermique_tuyau * capa_thermique_gaz) / (capa_thermique_tuyau * capa_thermique_gaz);

        if (chaleur > chaleur_max) {chaleur = chaleur_max;}

        long double changement_temp_tuyau = chaleur / capa_thermique_tuyau;
        long double changement_temp_gaz = (-chaleur) / capa_thermique_gaz;

        temp_metal += changement_temp_tuyau;
        temperature += changement_temp_gaz;
    }



    // ## échange tuyau <-> espace
    long double delta_tuyau_espace = temp_metal - espace.temperature;
    if (delta_tuyau_espace != 0) {
        long double chaleur = get_ft_metal_air() * delta_t;
        if (chaleur < 0) {chaleur = -chaleur;}

        // Calcule la chaleur maximum à transférer (cf. au-dessus)
        long double chaleur_max = (delta_tuyau_espace * capa_thermique_tuyau * capa_thermique_espace) / (capa_thermique_tuyau * capa_thermique_espace);


        if (chaleur > chaleur_max) {chaleur = chaleur_max;}

        long double changement_temp_tuyau = -chaleur / capa_thermique_tuyau;
        long double changement_temp_gaz = chaleur / capa_thermique_espace;


        temp_metal += changement_temp_tuyau;
        espace.temperature += changement_temp_gaz;
    }
}




long double EchangeurThermique::get_masse_tuyau() const {
    return Simulateur::get_masse_volumique(materiau) * volume_tuyau;
}






long double EchangeurThermique::get_gaz_ct() const {
    // Calcul du libre parcours moyen
    long double densite_part = this->quantite_mat * AVOGADRO / this->volume;
    long double diametre_part = Simulateur::get_diametre(simulateur->gaz);
    long double lpm = 1 / (SQRT_2 * densite_part * M_PI * diametre_part * diametre_part);

    // Calcul de la vitesse moyenne des particules
    long double vm = sqrt((8 * BOLTZMANN * this->temperature) / (M_PI * Simulateur::get_masse_moleculaire(this->simulateur->gaz)));

    // Calcul de la longueur caractéristique de l'échangeur.
    // La longueur caractéristique est une grandeur arbitraire, dépendante du contexte, etc
    // Ici, on prend le diamètre du cylindre.
    long double longueur_cara = this->diametre;

    // Le libre parcours moyen ne peut pas être plus petit que la longueur caractéristique
    // Cela introduit un comportement étonnant: la capacité thermique ne dépend pas de la pression,
    // sauf pour les pressions basses.
    if (lpm < longueur_cara) {lpm = longueur_cara;}

    // densité du gaz
    long double densite = this->quantite_mat * Simulateur::get_masse_molaire(simulateur->gaz) / this->volume;

    return Simulateur::get_ctm(simulateur->gaz) * densite * vm * lpm;
}



long double EchangeurThermique::get_ft_gaz_metal() const {
    // Normalement, il faudrait considérer ce transfert thermique comme une convection forcée.
    // Pour simplifier les calculs, on considère l'échange gaz → métal comme un échange conductif.
    return get_gaz_ct() * surface_interne * (this->temperature - this->temp_metal) / this->epaisseur;
}


long double EchangeurThermique::get_ft_metal_air() const {
    // On considère également ce transfert comme une conduction, pour des calculs plus simples.
    return Simulateur::get_ct(this->materiau) * surface_externe * (this->temp_metal - this->espace.temperature) / this->epaisseur;
}

long double EchangeurThermique::get_temp_espace() const {
    return this->espace.temperature;
}


inline void EchangeurThermique::update_nom() {
    this->nom = "Échangeur Thermique #" + std::to_string(this->id_element);
}

inline unsigned int EchangeurThermique::get_id_type() const {
    return ID_TYPE;
}

long double EchangeurThermique::get_temp_tuyau() const {
    return temp_metal;
}
