//
// Created by leo on 02/03/23.
//

#include "../../include/sim/Valve.h"

#include <stdexcept>


Valve::Valve(long double ouverture) {
    // Données membres de Valve
    if (ouverture < 0.0 || ouverture > 1.0) {
        throw std::runtime_error("L'ouverture d'une valve doit être entre 0 et 1 (compris)");
    }
    this->ouverture = ouverture;

    // données membres issues de Element
    this->simulateur = nullptr;
    this->id_element = 0;
}



void Valve::set_ouverture(long double o) {
    if (o < 0.0 || o > 1.0) {
        throw std::runtime_error("L'ouverture d'une valve doit être entre 0 et 1 (compris)");
    }
    this->ouverture = o;
}


long double Valve::get_ouverture() const {return ouverture;}



long double Valve::get_volume(unsigned int port) const {
    // Vérifie qu'un élément est bien connecté au port opposé
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'une valve");}
    if (!this->est_connecte(!port)) {throw std::runtime_error("Accès à une valve dont un port n'est pas connecté");}

    // appelle "get_volume" sur l'élément opposé (→ Valve sert juste de relai entre deux éléments)
    return this->connexions.at(!port).el->get_volume(!port);
}

long double Valve::get_pression(unsigned int port) const {
    // Vérifie qu'un élément est bien connecté au port opposé
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'une valve");}
    if (!this->est_connecte(!port)) {throw std::runtime_error("Accès à une valve dont un port n'est pas connecté");}

    // appelle "get_pression" sur l'élément opposé (→ Valve sert juste de relai entre deux éléments)
    return this->connexions.at(!port).el->get_pression(!port);
}

long double Valve::get_temp(unsigned int port) const {
    // Vérifie qu'un élément est bien connecté au port opposé
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'une valve");}
    if (!this->est_connecte(!port)) {throw std::runtime_error("Accès à une valve dont un port n'est pas connecté");}

    // appelle "get_temp" sur l'élément opposé (→ Valve sert juste de relai entre deux éléments)
    return this->connexions.at(!port).el->get_temp(!port);
}

long double Valve::get_mol(unsigned int port) const {
    // Vérifie qu'un élément est bien connecté au port opposé
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'une valve");}
    if (!this->est_connecte(!port)) {throw std::runtime_error("Accès à une valve dont un port n'est pas connecté");}

    // appelle "get_mol" sur l'élément opposé (→ Valve sert juste de relai entre deux éléments)
    return this->connexions.at(!port).el->get_mol(!port);
}

long double Valve::get_diametre(unsigned int port) const {
    // Vérifie qu'un élément est bien connecté au port opposé
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'une valve");}
    if (!this->est_connecte(!port)) {throw std::runtime_error("Accès à une valve dont un port n'est pas connecté");}
    if (!this->est_connecte(port)) {throw std::runtime_error("Accès à une valve dont un port n'est pas connecté");}

    // Renvoie le diamètre minimum des deux connexions à cette Valve, multiplié par son ouverture
    Connexion c1 = this->connexions.at(port);
    Connexion c2 = this->connexions.at(!port);
    long double diam = std::min(c1.el->get_diametre(c1.port), c2.el->get_diametre(c2.port));

    return diam * this->ouverture;
}

void Valve::transferer_matiere(long double quantite, long double temp, unsigned int port) {
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'une valve");}

    // appelle "transferer_matiere" sur l'élément opposé (→ Valve sert juste de relai entre deux éléments)
    unsigned int autre_port = !port;
    Connexion c = this->connexions.at(autre_port);
    c.el->transferer_matiere(quantite, temp, c.port);
}



inline void Valve::update_nom() {
    this->nom = "Valve #" + std::to_string(this->id_element);
}


unsigned int Valve::get_nb_ports() const {return NB_PORTS;}

inline unsigned int Valve::get_id_type() const {
    return ID_TYPE;
}
