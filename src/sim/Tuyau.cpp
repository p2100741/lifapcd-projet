//
// Created by leo on 02/03/23.
//

#include "../../include/sim/Tuyau.h"
#include "../../include/sim/Constantes.h"

#include <cmath>
#include <iostream>



Tuyau::Tuyau(long double diametre, long double longueur, long double pression, long double temp) {
    // empêche d'avoir un volume nul
    if (diametre <= 0) {throw std::runtime_error("Le diamètre d'un tuyau doit être > 0");}
    if (longueur <= 0) {throw std::runtime_error("La longueur d'un tuyau doit être > 0");}

    if (temp <= 0) {throw std::runtime_error("La température d'un réservoir doit être > 0");}
    if (pression < 0) {throw std::runtime_error("La pression d'un réservoir doit être >= 0");}

    // données membres propres à Tuyau
    this->longueur = longueur;

    // données membres issues de Contenant
    long double rayon = diametre / 2.0;
    this->volume = M_PI * rayon * rayon * longueur;
    this->diametre = diametre;
    this->temperature = temp;
    this->quantite_mat = (pression * this->volume) / (this->temperature * R);

    // données membres issues de Element
    this->simulateur = nullptr;
    this->id_element = 0;
}


unsigned int Tuyau::get_nb_ports() const {return NB_PORTS;}


long double Tuyau::get_longueur() const {return longueur;}


void Tuyau::transferer_matiere(long double quantite, long double temp, unsigned int port) {
    if (port >= NB_PORTS) {throw std::runtime_error("Accès à un port non-existant d'un tuyau");}


    // si quantite < 0, on doit s'assurer que this→quantite - quantite >= 0
    if (quantite < 0) {
        if (this->quantite_mat + quantite < 0) {
            quantite = -this->quantite_mat + 1e-4;
        }
    }

    // mise à jour des valeurs membres
    this->quantite_mat += quantite;
    this->temperature = get_nouvelle_temp(quantite, temp);
}




inline void Tuyau::update_nom() {
    this->nom = "Tuyau #" + std::to_string(this->id_element);
}

inline unsigned int Tuyau::get_id_type() const {
    return ID_TYPE;
}