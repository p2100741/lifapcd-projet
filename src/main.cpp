//
// Created by leo on 28/02/23.
//

#include <iostream>
#include <thread>

#include "../external/tclap/CmdLine.h" // Librairie pour la gestion de l'interface en ligne de commande
#include "../external/json.hpp"
using json = nlohmann::json;

#include "../external/imgui.h"
#include "../external/imgui_impl_sdl2.h"
#include "../external/imgui_impl_sdlrenderer.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../include/interface/EspaceTravail.h"
#include "../include/sim/ConstructeurSim.h"





std::string get_nom_fenetre(const EspaceTravail* et) {
    return et->nom + " - Gazoduc";
}




/**
 * @brief Affiche sur la sortie d'erreur le message spécifié si la condition n'est pas respectée,
 * puis termine le programme.
 * @param condition La condition à respecter
 * @param msg_erreur Le message à afficher
 */
void assert_erreur(bool condition, const char* msg_erreur) {
    if (!condition) {
        std::cerr << "\x1b[31mERREUR: " << msg_erreur << "\x1b[0m" << std::endl;
        exit(1);
    }
}







/**
 * @brief Lance l'interface du logiciel, en ouvrant le fichier spécifié. Si fichier = "", créé un nouveau fichier.
 * @param fichier Chemin vers le fichier à ouvrir, ou "" pour un nouveau fichier
 * @return le code de sortie de l'application
 */
int interface(const std::string& fichier) {
    // Setup SDL
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0)
    {
        printf("Error: %s\n", SDL_GetError());
        return -1;
    }
    if (TTF_Init() == -1) {
        printf("Error: %s\n", SDL_GetError());
        return -1;
    }


    // Chargement de la police d'écriture


    TTF_Font* font = TTF_OpenFont("data/font/Karla-Regular.ttf", 16);
    assert(font != nullptr);


    // définition de la structure répertoriant les textures
    // afin de la passer à EspaceTravail AVANT de charger les textures
    // on veut initier le renderer après le chargement de l'EspaceTravail
    TexturesElements textures;

    // Idem pour l'easter-egg
    TexturesEaster textures_easter;



    // Créé l'espace de travail
    std::string msg_erreur = "";
    EspaceTravail* et = nullptr;
    if (!fichier.empty()) {
        try {
            et = new EspaceTravail(fichier, &textures, font, &textures_easter);
        }
        catch (std::runtime_error& e) {
            et = new EspaceTravail(&textures, font, &textures_easter);
            msg_erreur = e.what();
        }
    }
    else {
        et = new EspaceTravail(&textures, font, &textures_easter);
    }




    SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_MAXIMIZED);
    SDL_Window* window = SDL_CreateWindow(get_nom_fenetre(et).c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    if (renderer == NULL)
    {
        SDL_Log("Error creating SDL_Renderer!");
        return 0;
    }



    // Chargement des textures
    textures.reservoir_text = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/reservoir.png"));
    assert(textures.reservoir_text != nullptr);
    textures.valve_text = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/valve.png"));
    assert(textures.valve_text != nullptr);
    textures.pompe_text = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/pompe.png"));
    assert(textures.pompe_text != nullptr);
    textures.echangeur_text = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/echangeurThm.png"));

    textures.tuyau_1x1_text = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/tuyau1x1.png"));
    assert(textures.tuyau_1x1_text != nullptr);
    textures.tuyau_1x3_text = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/tuyau1x3.png"));
    assert(textures.tuyau_1x3_text != nullptr);
    textures.tuyau_1x5_text = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/tuyau1x5.png"));
    assert(textures.tuyau_1x5_text != nullptr);
    textures.tuyau_2x2_text = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/tuyaucoude2x2.png"));
    assert(textures.tuyau_2x2_text != nullptr);
    textures.tuyau_4x4_text = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/tuyaucoude4x4.png"));
    assert(textures.tuyau_4x4_text != nullptr);

    textures_easter.toboggan_0 = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/toboggan-0.png"));
    assert(textures_easter.toboggan_0 != nullptr);
    textures_easter.toboggan_1 = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/toboggan-1.png"));
    assert(textures_easter.toboggan_1 != nullptr);
    textures_easter.toboggan_2 = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/toboggan-2.png"));
    assert(textures_easter.toboggan_2 != nullptr);
    textures_easter.toboggan_3 = SDL_CreateTextureFromSurface(renderer, IMG_Load("data/img/toboggan-3.png"));
    assert(textures_easter.toboggan_3 != nullptr);



     // Setup de Dear ImGui
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    io.Fonts->AddFontFromFileTTF("data/font/Karla-Regular.ttf", 16);

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();


    // Setup Platform/Renderer backends
    ImGui_ImplSDL2_InitForSDLRenderer(window, renderer);
    ImGui_ImplSDLRenderer_Init(renderer);

    bool done = false;
    Uint64 last_frame = SDL_GetTicks64();
    while (!done) // pour quitter la fenêtre
    {
        // mise à jour du nom de la fenêtre, si nécessaire
        if (SDL_GetWindowTitle(window) != get_nom_fenetre(et).c_str()) {
            SDL_SetWindowTitle(window, get_nom_fenetre(et).c_str());
        }


        Uint64 current_frame = SDL_GetTicks64();
        et->update(window, current_frame - last_frame);
        last_frame = current_frame;

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            ImGui_ImplSDL2_ProcessEvent(&event);    // traitement de l'événement par ImGui

            // on traite les événements seulement si ImGui ne les utilisent pas
            if (!io.WantCaptureMouse && !io.WantCaptureKeyboard) {
                et->evenement(event);
            }

            if (event.type == SDL_QUIT)
                done = true;
            if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window))
                done = true;
        }

        et->afficher(renderer, window);

        if (!msg_erreur.empty()) {
            et->ouvrir_popup(msg_erreur, true);
            msg_erreur.clear();
        }
    }

    // Cleanup
    ImGui_ImplSDLRenderer_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    TTF_CloseFont(font);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    delete et;

    return 0;
}













int main(int argc, char* argv[]) {
    srand(time(nullptr));


    // Accès aux arguments de la ligne de commande
    std::string chemin_fichier = "";
    std::string chemin_sortie = "";

    // valeurs par défauts
    long double duree = 10;
    long double intervalle = 0.1;
    bool ecraser = false;
    bool mode_texte = false;
    try {
        TCLAP::CmdLine cmd("Command description message", ' ', "1.0");

        std::string desc = "Chemin vers le fichier représentant le système à ouvrir";
        TCLAP::UnlabeledValueArg<std::string> nom_fichier_arg("fichier", desc, false, "", "CHEMIN");
        cmd.add(nom_fichier_arg);

        desc = "Spécifie une simulation en mode texte";
        TCLAP::SwitchArg mode_texte_arg("t", "texte", desc, cmd, mode_texte);

        desc = "Nom du fichier CSV à générer";
        TCLAP::ValueArg<std::string> nom_sortie_arg("s", "sortie", desc, false, "", "CHEMIN");
        cmd.add(nom_sortie_arg);

        desc = "Durée d'évolution du système à simuler, en secondes";
        TCLAP::ValueArg<long double> duree_arg("d", "duree", desc, false, duree, "DUREE");
        cmd.add(duree_arg);

        desc = "Intervalle entre chaque mesures de la simulation, en secondes. Doit être inférieur à la durée de la simulation";
        TCLAP::ValueArg<long double> intervalle_arg("i", "intervalle", desc, false, intervalle, "DUREE");
        cmd.add(intervalle_arg);

        desc = "Spécifie si un fichier du même nom que \"sortie\" sera écrasé ou non";
        TCLAP::SwitchArg ecrase_arg("e", "ecraser", desc, cmd, ecraser);


	    cmd.parse(argc, argv);

        chemin_fichier = nom_fichier_arg.getValue();
        chemin_sortie = nom_sortie_arg.getValue();
        duree = duree_arg.getValue();
        intervalle = intervalle_arg.getValue();
        ecraser = ecrase_arg;
        mode_texte = mode_texte_arg;


        assert_erreur(duree > 0, "La durée de simulation doit être supérieure à 0 seconde");
        assert_erreur(intervalle > 0, "La durée d'un intervalle doit être supérieure à 0 seconde");
        assert_erreur(duree > intervalle, "La durée d'un intervalle doit être inférieure à celle de simulation");
    }
    catch (TCLAP::ArgException& e) {
        std::cerr << "Erreur interne" << std::endl;
    }



    // lancement du mode graphique
    if (!mode_texte) {
        return interface(chemin_fichier);
    }


    // Mode textuel
    else {
        try {
            EspaceTravail esp = EspaceTravail(chemin_fichier, nullptr, nullptr, nullptr);

            // on récupère le nom du fichier (sans le chemin avant, ni l'extension)
            std::string nom_fichier = chemin_fichier.substr(0, chemin_fichier.find_last_of('.'));
            if (nom_fichier.find_last_of('/') != std::string::npos) {
                nom_fichier = nom_fichier.substr(nom_fichier.find_last_of('/'), chemin_fichier.length() - 1);
            }
            if (nom_fichier.find_last_of('\\') != std::string::npos) {
                nom_fichier = nom_fichier.substr(nom_fichier.find_last_of('\\'), chemin_fichier.length() - 1);
            }

            // définition d'un nom de fichier de sortie par défaut, d'après le nom du fichier d'entrée
            if (chemin_sortie.empty()) {
                chemin_sortie = nom_fichier + "_sortie.csv";
            }

            std::cout << "Construction de " << nom_fichier << "..." << std::endl;
            Simulateur* sim = construire_simulateur(esp);
            std::cout << "Simulation de " << nom_fichier << "..." << std::endl;
            sim->generer_csv(chemin_sortie, duree, intervalle, ecraser);
            std::cout << "Simulation terminée, le fichier " << chemin_sortie << " a été créé." << std::endl;

            delete sim;
        }
        catch (json::exception& e) {
            assert_erreur(false, "L'espace de travail est corrompu, des données sont manquantes/non-valides");
        }
        catch (std::runtime_error& e) {
            assert_erreur(false, e.what());
        }

    }
}