//
// Created by leo on 18/04/23.
//

#include "../../include/interface/TuyauGrphCourbe2x2.h"


TuyauGrphCourbe2x2::TuyauGrphCourbe2x2(unsigned int id): TuyauGraphique(id) {}

TuyauGrphCourbe2x2::TuyauGrphCourbe2x2(long double diametre, long double longueur, long double pression,
                       long double temp, Couple pos, unsigned int rota, unsigned int id)
                       : TuyauGraphique(diametre, longueur, pression, temp, pos, rota, id) {}

TuyauGrphCourbe2x2::TuyauGrphCourbe2x2(const json &j): TuyauGraphique(j) {}



const Couple& TuyauGrphCourbe2x2::get_dim() const {return DIMENSIONS;}
const std::vector<Couple> &TuyauGrphCourbe2x2::get_coo_ports() const {return COO_PORTS;}
const std::vector<unsigned int> &TuyauGrphCourbe2x2::get_orientation_ports() const {return ORIENT_PORTS;}
unsigned int TuyauGrphCourbe2x2::get_id_type() const {return TYPE_ID;}


const Couple TuyauGrphCourbe2x2::DIMENSIONS = {2,2};
const std::vector<Couple> TuyauGrphCourbe2x2::COO_PORTS = { Couple(0, 0), Couple(1, 1) };
const std::vector<unsigned int> TuyauGrphCourbe2x2::ORIENT_PORTS = {2, 1};