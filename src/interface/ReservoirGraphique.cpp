#include "../../include/interface/ReservoirGraphique.h"
#include "../../include/sim/Reservoir.h"
#include "../../include/sim/Constantes.h"


ReservoirGraphique::ReservoirGraphique(unsigned int id):
    volume(1),
    temp(TEMPERATURE_DEFAUT),
    pression(PRESSION_DEFAUT),
    diametre(0.05),
    immuable(false) {

    // données membres héritées de ElementGraphique
    this->rota = ROTA_DEFAUT;
    this->pos = POSITION_DEFAUT;
    this->id = id;
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}


ReservoirGraphique::ReservoirGraphique(long double volume, long double temp, long double pression, long double diametre,
                                       bool immuable, Couple pos, unsigned int rota, unsigned int id): volume(volume),
                                       temp(temp), pression(pression), diametre(diametre), immuable(immuable) {

    // données membres héritées de ElementGraphique
    this->rota = rota;
    this->pos = pos;
    this->id = id;
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}



ReservoirGraphique::ReservoirGraphique(const json &j) {
    this->id = j.at("id");
    this->pos = Couple(j.at("posX"), j.at("posY"));
    this->rota = j.at("rotation");
    this->volume = j.at("specs").at("volume");
    this->temp = j.at("specs").at("temp");
    this->pression = j.at("specs").at("pression");
    this->diametre = j.at("specs").at("diametre");
    this->immuable = j.at("specs").at("immuable");
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}




const Couple ReservoirGraphique::DIMENSIONS = {3,4};
const std::vector<Couple> ReservoirGraphique::COO_PORTS = { Couple(1, 3) };
const std::vector<unsigned int> ReservoirGraphique::ORIENT_PORTS = {0};

inline const Couple& ReservoirGraphique::get_dim() const {return DIMENSIONS;}
inline const std::vector<Couple>& ReservoirGraphique::get_coo_ports() const {return COO_PORTS;}
inline const std::vector<unsigned int>& ReservoirGraphique::get_orientation_ports() const {return ORIENT_PORTS;}
inline unsigned int ReservoirGraphique::get_id_type() const {return Reservoir::ID_TYPE;}


void ReservoirGraphique::afficher_fenetre(SDL_Renderer* renderer, SDL_Rect& rect){
    Reservoir * ptr = (Reservoir*) elt_correspd;

    std::string nom_fenetre = "Réservoir #" + std::to_string(id);
    ImGui::Begin(nom_fenetre.c_str(), &fenetre_affichee, FLAGS_FENETRE_DEFAUT);

    // trait liant l'élément à la fenêtre contenant ses informations
    ImVec2 centre_fenetre = ImGui::GetWindowPos();
    centre_fenetre += (ImGui::GetWindowSize() / 2);

    Couple centre_objet = Couple(rect.x, rect.y) + (Couple(rect.w, rect.h) / 2.0);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderDrawLine(renderer, centre_fenetre.x, centre_fenetre.y, centre_objet.x, centre_objet.y);




    // Volume du réservoir
    ImGui::Text("Volume :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double volume_t;
    if (ptr == nullptr) {volume_t = volume;}
    else {volume_t = ptr->get_volume(0);}
    if (ptr == nullptr) {
        ImGui::InputDouble("##volume", &volume_t, 0.001, 0.1, "%.3fm3");
        if(volume_t <= 0) {volume_t = 0.001;}
        volume = volume_t;
    }
    else {ImGui::InputDouble("##volume", &volume_t, 0.001, 0.1, "%.3fm3", ImGuiInputTextFlags_ReadOnly);}





    // Température du réservoir
    ImGui::Text("Température :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double temp_t;
    if (ptr == nullptr) {temp_t = temp;}
    else {temp_t = ptr->get_temp(0);}
    if (ptr == nullptr) {
        ImGui::InputDouble("##temp", &temp_t, 0.1, 1, "%.1fK");
        if(temp_t <= 0) {temp_t = 0.001;}
        temp = temp_t;
    }
    else {ImGui::InputDouble("##temp", &temp_t, 0.1, 1, "%.1fK", ImGuiInputTextFlags_ReadOnly);}





    // Pression du réservoir
    ImGui::Text("Pression :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double pression_t;
    if (ptr == nullptr) {pression_t = pression;}
    else {pression_t = ptr->get_pression(0);}
    if (ptr == nullptr) {
        ImGui::InputDouble("##pression", &pression_t, 1, 10, "%.0fPa");
        if(pression_t <= 0) {pression_t = 0.001;}
        pression = pression_t;
    }
    else {ImGui::InputDouble("##pression", &pression_t, 1, 10, "%.0fPa", ImGuiInputTextFlags_ReadOnly);}


   




    // Diamètre de la sortie du réservoir
    ImGui::Text("Diamètre :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double diam_t;
    if (ptr == nullptr) {diam_t = diametre;}
    else {diam_t = ptr->get_diametre(0);}
    if (ptr == nullptr) {
        ImGui::InputDouble("##diametre", &diam_t, 0.1, 1, "%.3fm");
        if(diam_t <= 0) {diam_t = 0.001;}
        diametre = diam_t;
    }
    else {ImGui::InputDouble("##diametre", &diam_t, 0.001, 0.01, "%.3fm", ImGuiInputTextFlags_ReadOnly);}





    // réglage réservoir immuable ou non
    if (ptr == nullptr) {
        ImGui::Checkbox("Immuable", &immuable);
    }
    // technique pour rendre la checkbox non modifiable
    else {
        bool dummy = immuable;
        ImGui::Checkbox("Immuable", &dummy);
        dummy = immuable;
    }





    // réglage rotation
    ImGui::Text("Rotation :");
    ImGui::SameLine(); // ↺ ↻
    if(ImGui::Button("<")) {
        if (ptr == nullptr) {this->rota = (this->rota + 1) % 4;}
    }
    ImGui::SameLine();
    if(ImGui::Button(">")) {
        if (ptr == nullptr) {this->rota = (this->rota - 1) % 4;}
    }


    ImGui::End();
}



json ReservoirGraphique::get_json() const {
    // on ajoute les valeurs une par une
    json j;
    j["type"] = get_id_type();
    j["id"] = id;
    j["posX"] = pos.x;
    j["posY"] = pos.y;
    j["rotation"] = rota;
    j["specs"]["volume"] = volume;
    j["specs"]["temp"] = temp;
    j["specs"]["pression"] = pression;
    j["specs"]["diametre"] = diametre;
    j["specs"]["immuable"] = immuable;

    return j;
}