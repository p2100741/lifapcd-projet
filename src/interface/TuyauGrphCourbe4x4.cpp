//
// Created by leo on 21/04/23.
//

#include "../../include/interface/TuyauGrphCourbe4x4.h"


TuyauGrphCourbe4x4::TuyauGrphCourbe4x4(unsigned int id): TuyauGraphique(id) {}

TuyauGrphCourbe4x4::TuyauGrphCourbe4x4(long double diametre, long double longueur, long double pression,
                       long double temp, Couple pos, unsigned int rota, unsigned int id)
                       : TuyauGraphique(diametre, longueur, pression, temp, pos, rota, id) {}

TuyauGrphCourbe4x4::TuyauGrphCourbe4x4(const json &j): TuyauGraphique(j) {}



const Couple& TuyauGrphCourbe4x4::get_dim() const {return DIMENSIONS;}
const std::vector<Couple> &TuyauGrphCourbe4x4::get_coo_ports() const {return COO_PORTS;}
const std::vector<unsigned int> &TuyauGrphCourbe4x4::get_orientation_ports() const {return ORIENT_PORTS;}
unsigned int TuyauGrphCourbe4x4::get_id_type() const {return TYPE_ID;}


const Couple TuyauGrphCourbe4x4::DIMENSIONS = {4,4};
const std::vector<Couple> TuyauGrphCourbe4x4::COO_PORTS = { Couple(0, 0), Couple(3, 3) };
const std::vector<unsigned int> TuyauGrphCourbe4x4::ORIENT_PORTS = {2, 1};