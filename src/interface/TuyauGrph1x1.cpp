//
// Created by leo on 18/04/23.
//

#include "../../include/interface/TuyauGrph1x1.h"


TuyauGrph1x1::TuyauGrph1x1(unsigned int id): TuyauGraphique(id) {}

TuyauGrph1x1::TuyauGrph1x1(long double diametre, long double longueur, long double pression,
                       long double temp, Couple pos, unsigned int rota, unsigned int id)
                       : TuyauGraphique(diametre, longueur, pression, temp, pos, rota, id) {}

TuyauGrph1x1::TuyauGrph1x1(const json &j): TuyauGraphique(j) {}



const Couple& TuyauGrph1x1::get_dim() const {return DIMENSIONS;}
const std::vector<Couple> &TuyauGrph1x1::get_coo_ports() const {return COO_PORTS;}
const std::vector<unsigned int> &TuyauGrph1x1::get_orientation_ports() const {return ORIENT_PORTS;}
unsigned int TuyauGrph1x1::get_id_type() const {return TYPE_ID;}


const Couple TuyauGrph1x1::DIMENSIONS = {1,1};
const std::vector<Couple> TuyauGrph1x1::COO_PORTS = { Couple(0, 0), Couple(0, 0) };
const std::vector<unsigned int> TuyauGrph1x1::ORIENT_PORTS = {2, 0};