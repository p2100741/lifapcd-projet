#include "../../include/interface/PompeGraphique.h"
#include "../../include/sim/Pompe.h"



PompeGraphique::PompeGraphique(unsigned int id):
    debit(0.001) {

    // données membres héritées de ElementGraphique
    this->rota = ROTA_DEFAUT;
    this->pos = POSITION_DEFAUT;
    this->id = id;
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}



PompeGraphique::PompeGraphique(long double debit, Couple pos, unsigned int rota, unsigned int id) {
    if (debit < 0.0) {
        throw std::runtime_error("Erreur dans les caractéristiques d'un élément (" + std::to_string(id) + ")");
    }

    this->debit = debit;
    this->pos = pos;
    this->rota = rota;
    this->id = id;
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}


PompeGraphique::PompeGraphique(const json &j) {
    this->id = j.at("id");
    this->pos = Couple(j.at("posX"), j.at("posY"));
    this->rota = j.at("rotation");

    long double dbt = j.at("specs").at("debit");
    if (dbt < 0.0) {
        throw std::runtime_error("Erreur dans les caractéristiques d'un élément (" + std::to_string(id) + ")");
    }
    this->debit = dbt;

    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}



const Couple PompeGraphique::DIMENSIONS = {3,3};
const std::vector<Couple> PompeGraphique::COO_PORTS = { Couple(0, 1), Couple(2, 1) };
const std::vector<unsigned int> PompeGraphique::ORIENT_PORTS = {3, 1};



inline const Couple& PompeGraphique::get_dim() const {return DIMENSIONS;}
inline const std::vector<Couple>& PompeGraphique::get_coo_ports() const {return COO_PORTS;}
inline const std::vector<unsigned int>& PompeGraphique::get_orientation_ports() const {return ORIENT_PORTS;}
inline unsigned int PompeGraphique::get_id_type() const {return Pompe::ID_TYPE;}



void PompeGraphique::afficher_fenetre(SDL_Renderer* renderer, SDL_Rect& rect){
    Pompe * ptr = (Pompe*) elt_correspd;

    std::string nom_fenetre = "Pompe #" + std::to_string(id);
    ImGui::Begin(nom_fenetre.c_str(), &fenetre_affichee, FLAGS_FENETRE_DEFAUT);

    // trait liant l'élément à la fenêtre contenant ses informations
    ImVec2 centre_fenetre = ImGui::GetWindowPos();
    centre_fenetre += (ImGui::GetWindowSize() / 2);

    Couple centre_objet = Couple(rect.x, rect.y) + (Couple(rect.w, rect.h) / 2.0);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderDrawLine(renderer, centre_fenetre.x, centre_fenetre.y, centre_objet.x, centre_objet.y);



    // Débit de la pompe, modifiable en cours de simulation
    ImGui::Text("Débit :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    if (ptr == nullptr) {
        float debit_t = debit;
        ImGui::SliderFloat("##", &debit_t, 0, 1.0, "%.3f m3/s", ImGuiSliderFlags_None);
        debit = debit_t;
    }
    else {
        float debit_t = ptr->get_debit();
        ImGui::SliderFloat("##", &debit_t, 0, 1.0, "%.3f m3/s", ImGuiSliderFlags_None);
        ptr->set_debit(debit_t);
    }





    // boutons de rotation
    ImGui::Text("Rotation :");
    ImGui::SameLine(); // ↺ ↻
    if(ImGui::Button("<")) {
        if (ptr == nullptr) {this->rota = (this->rota + 1) % 4;}
    }
    ImGui::SameLine();
    if(ImGui::Button(">")) {
        if (ptr == nullptr) {this->rota = (this->rota - 1) % 4;}
    }

    ImGui::End();
}


json PompeGraphique::get_json() const {
    // on ajoute les valeurs une par une
    json j;
    j["type"] = get_id_type();
    j["id"] = id;
    j["posX"] = pos.x;
    j["posY"] = pos.y;
    j["rotation"] = rota;
    j["specs"]["debit"] = debit;

    return j;
}