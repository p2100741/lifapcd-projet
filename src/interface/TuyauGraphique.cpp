#include "../../include/interface/TuyauGraphique.h"
#include "../../include/sim/Tuyau.h"
#include "../../include/sim/Constantes.h"

#define IMGUI_DEFINE_MATH_OPERATORS // actives les opérateurs d'ImGui


TuyauGraphique::TuyauGraphique(unsigned int id):
    diametre(0.05),
    longueur(0.5),
    pression(PRESSION_DEFAUT),
    temp(TEMPERATURE_DEFAUT) {

    // données membres héritées de ElementGraphique
    this->rota = ROTA_DEFAUT;
    this->pos = POSITION_DEFAUT;
    this->id = id;
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}


TuyauGraphique::TuyauGraphique(long double diametre, long double longueur, long double pression, long double temp,
                               Couple pos, unsigned int rota, unsigned int id): diametre(diametre), longueur(longueur),
                               pression(pression), temp(temp) {

    // données membres héritées de ElementGraphique
    this->rota = rota;
    this->pos = pos;
    this->id = id;
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}


TuyauGraphique::TuyauGraphique(const json &j) {
    this->id = j.at("id");
    this->pos = Couple(j.at("posX"), j.at("posY"));
    this->rota = j.at("rotation");
    this->temp = j.at("specs").at("temp");
    this->pression = j.at("specs").at("pression");
    this->diametre = j.at("specs").at("diametre");
    this->longueur = j.at("specs").at("longueur");
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}


TuyauGraphique::~TuyauGraphique() = default;



void TuyauGraphique::afficher_fenetre(SDL_Renderer* renderer, SDL_Rect& rect) {
    Tuyau * ptr = (Tuyau*) elt_correspd;

    std::string nom_fenetre = "Tuyau #" + std::to_string(id);
    ImGui::Begin(nom_fenetre.c_str(), &fenetre_affichee, FLAGS_FENETRE_DEFAUT);

    // trait liant l'élément à la fenêtre contenant ses informations
    ImVec2 centre_fenetre = ImGui::GetWindowPos();
    centre_fenetre += (ImGui::GetWindowSize() / 2);

    Couple centre_objet = Couple(rect.x, rect.y) + (Couple(rect.w, rect.h) / 2.0);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderDrawLine(renderer, centre_fenetre.x, centre_fenetre.y, centre_objet.x, centre_objet.y);





    // Longueur du tuyau
    ImGui::Text("Longueur :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double long_t;
    if (ptr == nullptr) {long_t = longueur;}
    else {long_t = ptr->get_longueur();}
    if (ptr == nullptr) {
        ImGui::InputDouble("##longueur", &long_t, 0.001, 0.1, "%.3fm");
        if(long_t <= 0) {long_t = 0.001;}
        longueur = long_t;
    }
    else {ImGui::InputDouble("##longueur", &long_t, 0.001, 0.1, "%.3fm", ImGuiInputTextFlags_ReadOnly);}





    // Diamètre du tuyau
    ImGui::Text("Diamètre :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double diam_t;
    if (ptr == nullptr) {diam_t = diametre;}
    else {diam_t = ptr->get_diametre(0);}
    if (ptr == nullptr) {
        ImGui::InputDouble("##diametre", &diam_t, 0.001, 0.1, "%.3fm");
        if(diam_t <= 0) {diam_t = 0.001;}
        diametre = diam_t;
    }
    else {ImGui::InputDouble("##diametre", &diam_t, 0.001, 0.1, "%.3fm", ImGuiInputTextFlags_ReadOnly);}






    // Température du tuyau
    ImGui::Text("Température :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double temp_t;
    if (ptr == nullptr) {temp_t = temp;}
    else {temp_t = ptr->get_temp(0);}
    if (ptr == nullptr) {
        ImGui::InputDouble("##temp", &temp_t, 0.1, 1, "%.3fK");
        if(temp_t <= 0) {temp_t = 0.001;}
        temp = temp_t;
    }
    else {ImGui::InputDouble("##temp", &temp_t, 0.1, 1, "%.3fK", ImGuiInputTextFlags_ReadOnly);}





    // Pression du tuyau
    ImGui::Text("Pression :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double pression_t;
    if (ptr == nullptr) {pression_t = pression;}
    else {pression_t = ptr->get_pression(0);}
    if (ptr == nullptr) {
        ImGui::InputDouble("##pression", &pression_t, 1, 10, "%.0fPa");
        if(pression_t <= 0) {pression_t = 0.001;}
        pression = pression_t;
    }
    else {ImGui::InputDouble("##pression", &pression_t, 1, 10, "%.0fPa", ImGuiInputTextFlags_ReadOnly);}









    // réglage rotation
    ImGui::Text("Rotation :");
    ImGui::SameLine(); // ↺ ↻
    if(ImGui::Button("<")) {
        if (ptr == nullptr) {this->rota = (this->rota + 1) % 4;}
    }
    ImGui::SameLine();
    if(ImGui::Button(">")) {
        if (ptr == nullptr) {this->rota = (this->rota - 1) % 4;}
    }

    ImGui::End();
}



json TuyauGraphique::get_json() const {
    // on ajoute les valeurs une par une
    json j;
    j["type"] = get_id_type();
    j["id"] = id;
    j["posX"] = pos.x;
    j["posY"] = pos.y;
    j["rotation"] = rota;
    j["specs"]["diametre"] = diametre;
    j["specs"]["longueur"] = longueur;
    j["specs"]["temp"] = temp;
    j["specs"]["pression"] = pression;

    return j;
}