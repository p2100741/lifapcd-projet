#include "../../include/interface/EchangeurThmGraphique.h"
#include "../../include/sim/EchangeurThermique.h"
#include "../../include/sim/Constantes.h"


EchangeurThmGraphique::EchangeurThmGraphique(unsigned int id):
    volume_espace(30),
    temperature_espace(TEMPERATURE_DEFAUT),
    espace_immuable(false),
    diametre(0.05),
    longueur(1),
    epaisseur(0.005),
    temperature(TEMPERATURE_DEFAUT),
    pression(PRESSION_DEFAUT),
    materiau(CUIVRE) {

    // données membres héritées de ElementGraphique
    this->rota = ROTA_DEFAUT;
    this->pos = POSITION_DEFAUT;
    this->id = id;
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}


EchangeurThmGraphique::EchangeurThmGraphique(long double ve, long double te, bool imm, long double diam,
                                             long double lon, long double ep, long double tg, long double pg, Materiau mat,
                                             Couple p, unsigned int r, unsigned int id): volume_espace(ve),
                                             temperature_espace(te), espace_immuable(imm), diametre(diam), longueur(lon),
                                             epaisseur(ep), temperature(tg), pression(pg), materiau(mat) {

    // données membres héritées de ElementGraphique
    this->rota = rota;
    this->pos = pos;
    this->id = id;
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}


EchangeurThmGraphique::EchangeurThmGraphique(const json &j) {
    this->id = j.at("id");
    this->pos = Couple(j.at("posX"), j.at("posY"));
    this->rota = j.at("rotation");

    this->volume_espace = j.at("specs").at("volume_espace");
    this->temperature_espace = j.at("specs").at("temperature_espace");
    this->espace_immuable = j.at("specs").at("espace_immuable");
    this->longueur = j.at("specs").at("longueur");
    this->diametre = j.at("specs").at("diametre");
    this->epaisseur = j.at("specs").at("epaisseur");
    this->temperature = j.at("specs").at("temperature");
    this->pression = j.at("specs").at("pression");

    this->materiau = static_cast<Materiau>(j.at("specs").at("materiau"));

    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}

const Couple EchangeurThmGraphique::DIMENSIONS = {6, 4};
const std::vector<Couple> EchangeurThmGraphique::COO_PORTS = { Couple(0, 1), Couple(5, 2) };
const std::vector<unsigned int> EchangeurThmGraphique::ORIENT_PORTS = {3, 1};


inline const Couple& EchangeurThmGraphique::get_dim() const {return DIMENSIONS;}
inline const std::vector<Couple>& EchangeurThmGraphique::get_coo_ports() const {return COO_PORTS;}
inline const std::vector<unsigned int>& EchangeurThmGraphique::get_orientation_ports() const {return ORIENT_PORTS;}
inline unsigned int EchangeurThmGraphique::get_id_type() const {return EchangeurThermique::ID_TYPE;}



void EchangeurThmGraphique::afficher_fenetre(SDL_Renderer *renderer, SDL_Rect &rect) {
    EchangeurThermique * ptr = (EchangeurThermique*) elt_correspd;

    std::string nom_fenetre = "Échangeur thermique #" + std::to_string(id);
    ImGui::Begin(nom_fenetre.c_str(), &fenetre_affichee, FLAGS_FENETRE_DEFAUT);




    // trait liant l'élément à la fenêtre contenant ses informations
    ImVec2 centre_fenetre = ImGui::GetWindowPos();
    centre_fenetre += (ImGui::GetWindowSize() / 2);

    Couple centre_objet = Couple(rect.x, rect.y) + (Couple(rect.w, rect.h) / 2.0);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderDrawLine(renderer, centre_fenetre.x, centre_fenetre.y, centre_objet.x, centre_objet.y);





    ImGui::Text("Extérieur");
    if (ImGui::BeginChild("Extérieur", ImVec2(270,100), true, ImGuiWindowFlags_AlwaysAutoResize)) {
        // Volume de l'espace extérieur
        ImGui::Text("Volume :");
        ImGui::SameLine();
        ImGui::PushItemWidth(150);
        // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
        // Sinon, on affiche simplement le volume de l'Element lié.
        double volume_t = volume_espace;
        if (ptr == nullptr) {
            ImGui::InputDouble("##volume", &volume_t, 0.001, 0.1, "%.3fm3");
            if(volume_t <= 0) {volume_t = 0.001;}
            volume_espace = volume_t;
        }
        else {ImGui::InputDouble("##volume", &volume_t, 0.001, 0.1, "%.3fm3", ImGuiInputTextFlags_ReadOnly);}


        // Température de l'espace extérieur
        ImGui::Text("Température :");
        ImGui::SameLine();
        ImGui::PushItemWidth(150);
        // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
        // Sinon, on affiche simplement le volume de l'Element lié.
        double temp_t = temperature_espace;
        if (ptr == nullptr) {
            ImGui::InputDouble("##temp_espace", &temp_t, 0.1, 1, "%.3fK");
            if(temp_t <= 0) {temp_t = 0.001;}
            temperature_espace = temp_t;
        }
        else {ImGui::InputDouble("##temp_espace", &temp_t, 0.1, 1, "%.3fK", ImGuiInputTextFlags_ReadOnly);}


        // réglage espace immuable ou non
        if (ptr == nullptr) {
            ImGui::Checkbox("Immuable", &espace_immuable);
        }
        // technique pour rendre la checkbox non modifiable
        else {
            bool dummy = espace_immuable;
            ImGui::Checkbox("Immuable", &dummy);
            dummy = espace_immuable;
        }
}


    ImGui::EndChild();




    ImGui::Text("Tuyau");
    if (ImGui::BeginChild("Tuyau", ImVec2(270,65), true, ImGuiWindowFlags_AlwaysAutoResize)) {


        // Choix du matériau
        ImGui::Text("Matériau :");
        ImGui::SameLine();
        ImGui::PushItemWidth(150);
        const char* materiaux_choix[] = {"Cuivre", "Acier", "Verre"};
        if (ImGui::BeginCombo("##materiaux", materiaux_choix[materiau])) {
            for (int i=0; i < 3; i++) {
                bool est_selectionne = (materiau == i);
                if (ImGui::Selectable(materiaux_choix[i], est_selectionne)) {
                    materiau = static_cast<Materiau>(i);
                }

                // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
                if (est_selectionne) {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndCombo();
        }





        // température du tuyau, non modifiable
        if (ptr == nullptr) {ImGui::Text("Température : %.3LfK", temperature_espace);}
        else {ImGui::Text("Température : %.3LfK", ptr->get_temp_tuyau());}
    }


    ImGui::EndChild();



    ImGui::Spacing();




    // Diamètre du tuyau
    ImGui::Text("Diamètre :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);
    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double diam_t = diametre;
    if (ptr == nullptr) {
        ImGui::InputDouble("##diametre", &diam_t, 0.1, 1, "%.3fm");
        if(diam_t <= 0) {diam_t = 0.001;}
        diametre = diam_t;
    }
    else {ImGui::InputDouble("##diametre", &diam_t, 0.001, 0.01, "%.3fm", ImGuiInputTextFlags_ReadOnly);}


    // Longueur du tuyau
    ImGui::Text("Longueur :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);
    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double long_t = longueur;
    if (ptr == nullptr) {
        ImGui::InputDouble("##longueur", &long_t, 0.1, 1, "%.3fm");
        if(diam_t <= 0) {diam_t = 0.001;}
        longueur = long_t;
    }
    else {ImGui::InputDouble("##longueur", &long_t, 0.001, 0.01, "%.3fm", ImGuiInputTextFlags_ReadOnly);}


    // Épaisseur du tuyau
    ImGui::Text("Épaisseur :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);
    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double epaiss_t = epaisseur;
    if (ptr == nullptr) {
        ImGui::InputDouble("##epaisseur", &epaiss_t, 0.1, 1, "%.3fm");
        if(epaiss_t <= 0) {epaiss_t = 0.001;}
        epaisseur = epaiss_t;
    }
    else {ImGui::InputDouble("##epaisseur", &epaiss_t, 0.001, 0.01, "%.3fm", ImGuiInputTextFlags_ReadOnly);}





    // Température du gaz contenu
    ImGui::Text("Température :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double temp_t;
    if (ptr == nullptr) {temp_t = temperature;}
    else {temp_t = ptr->get_temp(0);}
    if (ptr == nullptr) {
        ImGui::InputDouble("##temp", &temp_t, 0.1, 1, "%.3fK");
        if(temp_t <= 0) {temp_t = 0.001;}
        temperature = temp_t;
    }
    else {ImGui::InputDouble("##temp", &temp_t, 0.1, 1, "%.3fK", ImGuiInputTextFlags_ReadOnly);}




    // Pression du gaz
    ImGui::Text("Pression :");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);

    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    double pression_t;
    if (ptr == nullptr) {pression_t = pression;}
    else {pression_t = ptr->get_pression(0);}
    if (ptr == nullptr) {
        ImGui::InputDouble("##pression", &pression_t, 1, 10, "%.0fPa");
        if(pression_t <= 0) {pression_t = 0.001;}
        pression = pression_t;
    }
    else {ImGui::InputDouble("##pression", &pression_t, 1, 10, "%.0fPa", ImGuiInputTextFlags_ReadOnly);}





    


    // réglage rotation
    ImGui::Text("Rotation :");
    ImGui::SameLine(); // ↺ ↻
    if(ImGui::Button("<")) {
        if (ptr == nullptr) {this->rota = (this->rota + 1) % 4;}
    }
    ImGui::SameLine();
    if(ImGui::Button(">")) {
        if (ptr == nullptr) {this->rota = (this->rota - 1) % 4;}
    }

    ImGui::End();
}



json EchangeurThmGraphique::get_json() const {
    json j;
    j["type"] = get_id_type();
    j["id"] = id;
    j["posX"] = pos.x;
    j["posY"] = pos.y;
    j["rotation"] = rota;
    j["specs"]["volume_espace"] = volume_espace;
    j["specs"]["temperature_espace"] = temperature_espace;
    j["specs"]["espace_immuable"] = espace_immuable;
    j["specs"]["longueur"] = longueur;
    j["specs"]["diametre"] = diametre;
    j["specs"]["epaisseur"] = epaisseur;
    j["specs"]["materiau"] = materiau;
    j["specs"]["temperature"] = temperature;
    j["specs"]["pression"] = pression;


    return j;
}