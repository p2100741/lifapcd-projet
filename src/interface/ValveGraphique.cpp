#include "../../include/interface/ValveGraphique.h"
#include "../../include/sim/Valve.h"


ValveGraphique::ValveGraphique(unsigned int id):
    ouverture(1.0) {

    // données membres héritées de ElementGraphique
    this->rota = ROTA_DEFAUT;
    this->pos = POSITION_DEFAUT;
    this->id = id;
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}



ValveGraphique::ValveGraphique(long double ouverture, Couple pos, unsigned int rota, unsigned int id) {
    if (ouverture < 0.0 || ouverture > 1.0) {
        throw std::runtime_error("Erreur dans les caractéristiques d'un élément (" + std::to_string(id) + ")");
    }
    this->ouverture = ouverture;
    this->pos = pos;
    this->rota = rota;
    this->id = id;
    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}


ValveGraphique::ValveGraphique(const json &j) {
    this->id = j.at("id");
    this->pos = Couple(j.at("posX"), j.at("posY"));
    this->rota = j.at("rotation");

    long double ovrt = j.at("specs").at("ouverture");
    if (ovrt < 0.0 || ovrt > 1.0) {
        throw std::runtime_error("Erreur dans les caractéristiques d'un élément (" + std::to_string(id) + ")");
    }
    this->ouverture = ovrt;

    this->elt_correspd = nullptr;
    this->fenetre_affichee = false;
}



const Couple ValveGraphique::DIMENSIONS = {1,3};
const std::vector<Couple> ValveGraphique::COO_PORTS = { Couple(0, 1), Couple(0, 1) };
const std::vector<unsigned int> ValveGraphique::ORIENT_PORTS = {1, 3};

inline const Couple& ValveGraphique::get_dim() const {return DIMENSIONS;}
inline const std::vector<Couple>& ValveGraphique::get_coo_ports() const {return COO_PORTS;}
inline const std::vector<unsigned int>& ValveGraphique::get_orientation_ports() const {return ORIENT_PORTS;}
inline unsigned int ValveGraphique::get_id_type() const {return Valve::ID_TYPE;}



void ValveGraphique::afficher_fenetre(SDL_Renderer* renderer, SDL_Rect& rect) {
    auto* ptr = (Valve*) elt_correspd;

    std::string nom_fenetre = "Valve #" + std::to_string(id);
    ImGui::Begin(nom_fenetre.c_str(), &fenetre_affichee, FLAGS_FENETRE_DEFAUT);

    // on affiche d'abord, via SDL, un trait entre le centre de la fenêtre et le centre de l'objet
    ImVec2 centre_fenetre = ImGui::GetWindowPos();
    centre_fenetre += (ImGui::GetWindowSize() / 2);

    Couple centre_objet = Couple(rect.x, rect.y) + (Couple(rect.w, rect.h) / 2.0);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderDrawLine(renderer, centre_fenetre.x, centre_fenetre.y, centre_objet.x, centre_objet.y);







    // S'il n'y a pas de lien vers un Element, on permet l'édition de la valeur volume
    // Sinon, on affiche simplement le volume de l'Element lié.
    if (ptr == nullptr) {
        int ouverture_pourcent = ouverture * 100;
        ImGui::Text("Ouverture :");
        ImGui::SameLine();
        ImGui::PushItemWidth(150);
        ImGui::SliderInt("##", &ouverture_pourcent, 0.0, 100.0, "%d%%", ImGuiSliderFlags_AlwaysClamp);
        ouverture = ouverture_pourcent / 100.0;
    }
    else {
        int ouverture_pourcent = ptr->get_ouverture() * 100;
        ImGui::Text("Ouverture :");
        ImGui::SameLine();
        ImGui::PushItemWidth(150);
        ImGui::SliderInt("##", &ouverture_pourcent, 0.0, 100.0, "%d%%", ImGuiSliderFlags_AlwaysClamp);
        ptr->set_ouverture(ouverture_pourcent / 100.0);
    }







    // boutons de rotation
    ImGui::Text("Rotation :");
    ImGui::SameLine(); // ↺ ↻
    if(ImGui::Button("<")) {
        if (ptr == nullptr) {this->rota = (this->rota + 1) % 4;}
    }
    ImGui::SameLine();
    if(ImGui::Button(">")) {
        if (ptr == nullptr) {this->rota = (this->rota - 1) % 4;}
    }

    ImGui::End();
}



json ValveGraphique::get_json() const {
    // on ajoute les valeurs une par une
    json j;
    j["type"] = get_id_type();
    j["id"] = id;
    j["posX"] = pos.x;
    j["posY"] = pos.y;
    j["rotation"] = rota;
    j["specs"]["ouverture"] = ouverture;

    return j;
}
