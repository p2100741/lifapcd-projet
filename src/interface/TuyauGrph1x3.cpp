//
// Created by leo on 18/04/23.
//

#include "../../include/interface/TuyauGrph1x3.h"

TuyauGrph1x3::TuyauGrph1x3(unsigned int id): TuyauGraphique(id) {}

TuyauGrph1x3::TuyauGrph1x3(long double diametre, long double longueur, long double pression,
                       long double temp, Couple pos, unsigned int rota, unsigned int id)
                       : TuyauGraphique(diametre, longueur, pression, temp, pos, rota, id) {}

TuyauGrph1x3::TuyauGrph1x3(const json &j): TuyauGraphique(j) {}



const Couple& TuyauGrph1x3::get_dim() const {return DIMENSIONS;}
const std::vector<Couple> &TuyauGrph1x3::get_coo_ports() const {return COO_PORTS;}
const std::vector<unsigned int> &TuyauGrph1x3::get_orientation_ports() const {return ORIENT_PORTS;}
unsigned int TuyauGrph1x3::get_id_type() const {return TYPE_ID;}


const Couple TuyauGrph1x3::DIMENSIONS = {1,3};
const std::vector<Couple> TuyauGrph1x3::COO_PORTS = { Couple(0, 0), Couple(0, 2) };
const std::vector<unsigned int> TuyauGrph1x3::ORIENT_PORTS = {2, 0};
