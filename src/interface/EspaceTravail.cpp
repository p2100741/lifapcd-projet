#include "../../include/interface/EspaceTravail.h"

#include "../../include/interface/ReservoirGraphique.h"
#include "../../include/interface/ValveGraphique.h"

#include "../../include/interface/TuyauGrph1x1.h"
#include "../../include/interface/TuyauGrph1x3.h"
#include "../../include/interface/TuyauGrph1x5.h"
#include "../../include/interface/TuyauGrphCourbe2x2.h"
#include "../../include/interface/TuyauGrphCourbe4x4.h"


#include "../../include/interface/PompeGraphique.h"
#include "../../include/interface/EchangeurThmGraphique.h"

#include "../include/sim/ConstructeurSim.h"


#include "../../external/imgui_impl_sdl2.h"
#include "../../external/imgui_impl_sdlrenderer.h"
#include "../../external/imgui_stdlib.h"

#include <SDL2/SDL_ttf.h>

#include <fstream>
#include <stdexcept>
#include <iostream>


const float ZOOM_MIN = 0.1;
const float ZOOM_MAX = 10;

const unsigned int COO_X_ZONE_SUPPRESSION_ELEMENT = 220;

using json = nlohmann::json;


/**
 * @brief Calcule et renvoie le modulo euclidien de x par y.
 * Pour des nombres positifs, c'est équivalent à x % y, mais pour x négatif, le résultat sera différent.
 * @param x
 * @param y
 * @return x mod y
 */
unsigned int modulo(int x, unsigned int y) {
    int rem = x;
    while (rem < 0) {rem += y;}
    while (rem > y) {rem -= y;}
    return rem;
}




Couple get_centre_fenetre(SDL_Window * window) {
    int w = SDL_GetWindowSurface(window)->w;
    int h = SDL_GetWindowSurface(window)->h;
    return Couple(w/2,h/2);
}






EspaceTravail::EspaceTravail(TexturesElements* t, TTF_Font* f, TexturesEaster* te): textures(t), font(f), tex_easter(te), win(nullptr), sim(nullptr) {
    nom = "Sans nom";
    gaz = 0;
    grille_affichee = true;
    zoom = 1.0;
    decalX = 0.0;
    decalY = 0.0;
    chemin_fichier = "";
}

EspaceTravail::~EspaceTravail() {
    for (auto el: element) {
        delete el.second;
    }
}

EspaceTravail::EspaceTravail(const std::string& nom_fichier, TexturesElements* t, TTF_Font* f, TexturesEaster* te): textures(t), font(f), tex_easter(te), win(nullptr), sim(nullptr) {
    charger_fichier(nom_fichier);
}




// {
//            element.erase(ptr->id);
//            delete ptr;
//        }



ElementGraphique& EspaceTravail::get_element(unsigned int id) const{
    // Vérifie que l'élément existe
    if (this->element.find(id) == this->element.end()) {
        std::cerr << "Element non trouvé" << std::endl;
        assert(0);
    }

    return *this->element.at(id);
}






void EspaceTravail::charger_fichier(const std::string& nom_fichier) {
    // ouvre le fichier spécifié
    std::ifstream fichier (nom_fichier);

    if (!fichier.is_open()) {
        throw std::runtime_error("Impossible d'ouvrir le fichier (" + nom_fichier + ")");
    }

    // on charge le json depuis le fichier
    json espace_travail = json::parse(fichier);
    fichier.close();


    // on récupère toutes les informations avant de les appliquer à l'EspaceTravail afin de pouvoir se remettre
    // d'une erreur
    std::string nom_t = espace_travail.at("nom_espace");
    unsigned int gaz_t = espace_travail.at("gaz");
    float zoom_t = espace_travail.at("zoom");
    float decalX_t = espace_travail.at("decalX");
    float decalY_t = espace_travail.at("decalY");
    std::unordered_map<unsigned int, ElementGraphique*> element_t;

    // On itère sur tous les éléments. En fonction de leur type,
    // on construit un ElementGraphique différent.
    for (auto& el: espace_travail.at("elements")) {
        switch (el.at("type").get<unsigned int>()) {

            case 10:
                element_t.insert(std::make_pair(el.at("id"), new ReservoirGraphique(el)));
                break;

            case 11:
                element_t.insert(std::make_pair(el.at("id"), new EchangeurThmGraphique(el)));
                break;

            case 12:
                element_t.insert(std::make_pair(el.at("id"), new ValveGraphique(el)));
                break;

            case 13:
                element_t.insert(std::make_pair(el.at("id"), new PompeGraphique(el)));
                break;


            // Les tuyaux sont dans les 20aines.
            case 20:
                element_t.insert(std::make_pair(el.at("id"), new TuyauGrph1x3(el)));
                break;
            case 21:
                element_t.insert(std::make_pair(el.at("id"), new TuyauGrph1x5(el)));
                break;
            case 22:
                element_t.insert(std::make_pair(el.at("id"), new TuyauGrphCourbe2x2(el)));
                break;
            case 23:
                element_t.insert(std::make_pair(el.at("id"), new TuyauGrphCourbe4x4(el)));
                break;
            case 24:
                element_t.insert(std::make_pair(el.at("id"), new TuyauGrph1x1(el)));
                break;

            default: {
                std::cerr << "Type d'élément non trouvé" << std::endl;
                assert(0);
            }
        }
    }



    // si à ce stade aucune erreur ne s'est produit, on peut modifier les valeurs sans crainte de problèmes
    this->nom = nom_t;
    this->gaz = gaz_t;
    this->zoom = zoom_t;
    this->decalX = decalX_t;
    this->decalY = decalY_t;
    this->grille_affichee = true;

    this->chemin_fichier = nom_fichier;

    supprimer_simulation();
    // désalloue les éléments graphiques
    for (auto el: element) {
        delete el.second;
    }
    element.clear();

    element = element_t;
}


void EspaceTravail::vider() {
    supprimer_simulation();

    // désalloue les éléments graphiques
    for (auto el: element) {
        delete el.second;
    }
    element.clear();


    nom = "Sans nom";
    gaz = 0;
    grille_affichee = true;
    zoom = 1.0;
    decalX = 0.0;
    decalY = 0.0;
    chemin_fichier = "";
}





void EspaceTravail::evenement(const SDL_Event& event) {
    // un clic:
    // - sur un élement -> soit un clic (= on ouvre sa fenêtre), soit un maintient (= on le déplace)
    //                     (un clic => delta_el_attrape = 0)
    // - sur l'espace -> déplacement de l'espace
    if (event.type == SDL_MOUSEBUTTONDOWN) {

        ElementGraphique* ptr = get_element_survole(Couple(event.button.x, event.button.y));


        // début translation de l'Espace de travail
        if (ptr == nullptr) {
            translation = true;
            translation_delta_x = 0, translation_delta_y = 0;
            Couple pos_souris = Couple(0,0);
            SDL_GetMouseState(&pos_souris.x, &pos_souris.y);
            translation_origin_x = pos_souris.x, translation_origin_y = pos_souris.y;
        }

        // début déplacement element / ouverture de la fenêtre (ouverture de fenêtre = déplacement de 0 px)
        else {
            ptr_el_attrape = ptr;
            SDL_GetMouseState(&attrape_coo_souris.x, &attrape_coo_souris.y);
            delta_el_attrape = get_coo_souris_grille() - ptr_el_attrape->pos;
        }
    }

    // fin d'une translation = application de cette translation
    else if (event.type == SDL_MOUSEBUTTONUP && translation) {
        translation = false;

        decalX += translation_delta_x;
        decalY += translation_delta_y;

        translation_delta_x = 0, translation_delta_y = 0;
    }

    // fin d'un déplacement d'élément
    else if (event.type == SDL_MOUSEBUTTONUP && ptr_el_attrape != nullptr) {
        Couple coo_souris = Couple(0, 0);
        SDL_GetMouseState(&coo_souris.x, &coo_souris.y);

        // glissage d'un élément dans la zone de suppression
        if (coo_souris.x < COO_X_ZONE_SUPPRESSION_ELEMENT) {
            supprimer_element(ptr_el_attrape);
        }

        // On ne supprime pas l'élément, il est simplement déplacé
        else {
            Couple delta = coo_souris - attrape_coo_souris;
            if (delta.x == 0 && delta.y == 0) {
                ptr_el_attrape->fenetre_affichee = true;
            }
        }

        ptr_el_attrape = nullptr;
    }


    // zoom
    else if (event.type == SDL_MOUSEWHEEL) {
        zoom += zoom * 0.1 * event.wheel.y; // event.wheel.y: 1 ou -1 en fonction de la direction
        if (zoom < ZOOM_MIN) {zoom = ZOOM_MIN;}
        else if (zoom > ZOOM_MAX) {zoom = ZOOM_MAX;}
    }



    // Entrées clavier
    else if (event.type == SDL_KEYDOWN) {
        auto key_event = (SDL_KeyboardEvent&) event;

        // Ctrl + S => enregistrer ou enregistrer-sous
        if (key_event.keysym.scancode == SDL_SCANCODE_S && key_event.keysym.mod == KMOD_LCTRL) {
            if (!chemin_fichier.empty()) {
                try {
                    sauvegarder(chemin_fichier);
                    ouvrir_popup("L'espace de travail a bien été enregistré", false);
                }
                catch(std::runtime_error& e) {ouvrir_popup(e.what(), true);}
            }
            else {
                menu_popup_a_ouvrir = 3;
            }
        }
    }
}







void EspaceTravail::update(SDL_Window* window, Uint64 delta_t) {
    this->win = window;

    // milieu d'une translation de la vue
    if (translation) {
        // calcul du vecteur de déplacement, en pixels
        Couple pos_souris = Couple(0,0);
        SDL_GetMouseState(&pos_souris.x, &pos_souris.y);
        translation_delta_x = pos_souris.x - translation_origin_x;
        translation_delta_y = pos_souris.y - translation_origin_y;

        // conversion du delta en terme de cases
        unsigned int vraie_taille_grille = taille_grille * zoom;
        translation_delta_x = translation_delta_x / vraie_taille_grille;
        translation_delta_y = translation_delta_y / vraie_taille_grille;
    }


    // milieu d'un déplacement d'élément, on ne déplace que si la simulation ne tourne pas
    if (ptr_el_attrape != nullptr && sim == nullptr) {
        ptr_el_attrape->pos = get_coo_souris_grille() - delta_el_attrape;
    }

    if (sim == nullptr) {pause = true;}

    // avancement de la simulation
    else if (!pause) {
        long double delta = delta_t  * modificateur_vitesse;
        temps_ecoule += delta / 1000.0;

        // en cas d'exception lors de la simulation, on arrête tout et on affiche une pop-up d'erreur
        try {
            sim->simuler_etape(delta);
        }
        catch (std::runtime_error& e) {
            supprimer_simulation();
            ouvrir_popup(e.what(), true);
        }
    }
}











void EspaceTravail::dessiner_grille(SDL_Renderer* renderer, Couple c) const {

    // rend la grille transparente au fur et à mesure du dézoomage
    unsigned int opa = 50;
    if (zoom < 1) {opa = 63 * zoom - 12.5;}
    if (zoom < 0.2) {opa = 0;}

    unsigned int vraie_taille_grille = taille_grille * zoom;

    int x_debut = modulo(c.x, vraie_taille_grille);
    int y_debut = modulo(c.y, vraie_taille_grille);


    int w = SDL_GetWindowSurface(win)->w;
    int h = SDL_GetWindowSurface(win)->h;

    // dessine l'origine de la grille
    SDL_Rect centre;
    centre.x = (vraie_taille_grille/2) + c.x - 2;
    centre.y = (vraie_taille_grille/2) + c.y - 2;
    centre.h = 5;
    centre.w = 5;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, opa); // couleur de la grille

    SDL_RenderFillRect(renderer, &centre);

    for(int x = x_debut; x < w ; x+= vraie_taille_grille){
        SDL_RenderDrawLine(renderer, x, 0, x, h); // dessine les lignes verticales de la grille
    }

    for(int y = y_debut; y < h; y+= vraie_taille_grille){
        SDL_RenderDrawLine(renderer, 0, y, w, y); // dessine les lignes horizontales de la grille
    }
}





/** Renvoie la texture correspondant à l'élément */
SDL_Texture* EspaceTravail::get_texture(const ElementGraphique* el) const {
    SDL_Texture * texture;
    switch(el->get_id_type()) {
        case 10: texture = textures->reservoir_text; break;
        case 11: texture = textures->echangeur_text; break;
        case 12: texture = textures->valve_text; break;
        case 13: texture = textures->pompe_text; break;

        // tuyaux
        case 20: texture = textures->tuyau_1x3_text; break;
        case 21: texture = textures->tuyau_1x5_text; break;
        case 22: texture = textures->tuyau_2x2_text; break;
        case 23: texture = textures->tuyau_4x4_text; break;
        case 24: texture = textures->tuyau_1x1_text; break;

        default : {
            std::cerr << "Texture d'élément non-trouvée" << std::endl;
            assert(0);
        }
    }

    return texture;
}



SDL_Rect EspaceTravail::get_rectangle_element(const ElementGraphique *el, Couple c) const {
    unsigned int vraie_taille_grille = taille_grille * zoom;

    // détermine le rectangle dans lequel dessiner l'élément
    SDL_Rect rect;
    rect.x = c.x + el->pos.x*vraie_taille_grille;
    rect.y = c.y + el->pos.y*vraie_taille_grille;

    Couple dim = el->get_dim();

    rect.w = dim.x * vraie_taille_grille;
    rect.h = dim.y * vraie_taille_grille;

    if (el->rota == 1) {
        rect.y -= (dim.x - 1) * vraie_taille_grille;
        rect.h = dim.x * vraie_taille_grille;
        rect.w = dim.y * vraie_taille_grille;
    }
    if (el->rota == 2) {
        rect.x -= (dim.x - 1) * vraie_taille_grille;
        rect.y -= (dim.y - 1) * vraie_taille_grille;
    }
    if (el->rota == 3) {
        rect.x -= (dim.y - 1) * vraie_taille_grille;
        rect.h = dim.x * vraie_taille_grille;
        rect.w = dim.y * vraie_taille_grille;
    }

    return rect;
}






void EspaceTravail::dessiner_element(SDL_Renderer *renderer, Couple c, ElementGraphique *el) const {
    unsigned int vraie_taille_grille = taille_grille * zoom;
    SDL_Texture* texture = get_texture(el);

    // détermine le rectangle dans lequel dessiner l'élément
    SDL_Rect rect;
    rect.x = c.x + el->pos.x*vraie_taille_grille;
    rect.y = c.y + el->pos.y*vraie_taille_grille;

    Couple dim = el->get_dim();

    rect.w = dim.x * vraie_taille_grille;
    rect.h = dim.y * vraie_taille_grille;

    SDL_Point centre_rota_text;
    centre_rota_text.x = vraie_taille_grille / 2;
    centre_rota_text.y = vraie_taille_grille / 2;

    //SDL_RenderCopy(renderer, texture, nullptr, &rect);
    SDL_RenderCopyEx(renderer, texture, nullptr, &rect, el->rota * (270), &centre_rota_text, SDL_FLIP_NONE);
}






void EspaceTravail::afficher(SDL_Renderer* renderer, SDL_Window* window) {
    ImGui_ImplSDLRenderer_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();
    
    unsigned int vraie_taille_grille = taille_grille * zoom;

    if (sim == nullptr) {SDL_SetRenderDrawColor(renderer, 30, 50, 60, 255);}
    else {SDL_SetRenderDrawColor(renderer, 30, 30, 30, 255);}
    SDL_RenderClear(renderer);

    Couple c = get_origine();

    if (grille_affichee) {dessiner_grille(renderer, c);}


    // affichage de chaque élément
    for (auto& el: element){
        dessiner_element(renderer, c, el.second);
    }


    // affichage de la fenêtre de chaque élément
    SDL_Rect r;
    for (auto& el: element){
        if (el.second->fenetre_affichee) {
            r = get_rectangle_element(el.second, c);
            el.second->afficher_fenetre(renderer, r);
        }
    }




    // dessine la case où se trouve la souris, ou le rectangle de l'élément survolé
    Couple coo_souris = Couple(0,0);
    SDL_GetMouseState(&coo_souris.x, &coo_souris.y);
    ElementGraphique* el_survole = get_element_survole(coo_souris);

    if (el_survole == nullptr) {
        Couple pos_souris = get_coo_souris_grille();
        r.x = c.x + (vraie_taille_grille * pos_souris.x);
        r.y = c.y + (vraie_taille_grille * pos_souris.y);
        r.w = vraie_taille_grille;
        r.h = vraie_taille_grille;
        SDL_RenderDrawRect(renderer, &r);
        r.x += 1;
        r.y += 1;
        r.w -= 2;
        r.h -= 2;
        SDL_RenderDrawRect(renderer, &r);
    }
    else {
        r = get_rectangle_element(el_survole, c);
        SDL_RenderDrawRect(renderer, &r);
    }


    // affiche les fenêtres de l'IU
    afficher_controles_sim(window);
    if (pop_up_affichee) {afficher_popup(window);}

    afficher_menu(window);




    /*
    // affiche des informations sur le zoom et le décalage en bas à droite de la fenêtre
    SDL_Color txt_color = {255, 255, 255, 50};
    std::string text_zoom = std::to_string(zoom) + "x";
    std::string text_decal = std::to_string(decalX + translation_delta_x) + " " + std::to_string(decalY + translation_delta_y);
    SDL_Surface* info_txt_surf = TTF_RenderText_Solid(font, text.c_str(), txt_color);
    SDL_Texture* info_txt_text = SDL_CreateTextureFromSurface(renderer, info_txt_surf);

    int window_w, window_h;
    SDL_GetWindowSize(window, &window_w, &window_h);

    SDL_Rect rect_info_txt;
    rect_info_txt.x = window_w - info_txt_surf->w;
    rect_info_txt.y = window_h - info_txt_surf->h;
    rect_info_txt.w = info_txt_surf->w;
    rect_info_txt.h = info_txt_surf->h;

    SDL_RenderCopy(renderer, info_txt_text, nullptr, &rect_info_txt);

    // clean-up des surfaces/textures générées
    SDL_FreeSurface(info_txt_surf);
    SDL_DestroyTexture(info_txt_text);
     */

    // Rendering
    ImGui::Render();
    //SDL_RenderSetScale(renderer, io.DisplayFramebufferScale.x, io.DisplayFramebufferScale.y);

    ImGui_ImplSDLRenderer_RenderDrawData(ImGui::GetDrawData());
    SDL_RenderPresent(renderer); // affiche une nouvelle image
}





void EspaceTravail::ajouter_element(unsigned int type_el) {
    // on s'assure que le type d'élément existe
    unsigned int types_possibles[7] = {10, 11, 12, 13, 20, 21};
    if (!std::find(std::begin(types_possibles), std::end(types_possibles), type_el)) {
        throw std::runtime_error("Ce type d'élément n'existe pas");
    }

    // S'il y a plus de 100 éléments dans l'ET, on renvoie une erreur, car c'est la limite de simulation.
    if (element.size() == 100) {
        throw std::runtime_error("La limite d'éléments a été atteinte");
    }

    // Cherche un ID libre à attribuer à l'élément. On est certain qu'il en existe un,
    // car chaque type d'élément peut avoir 100 ids différents et il n'y a pas plus de 100 éléments.
    unsigned int id;
    do {
        // génère un ID de la forme AABB, avec AA = type de l'élément et BB un nombre aléatoire
        id = type_el * 100 + (rand() % 100);
    } while (element.count(id) == 1);


    // création de l'élément en fonction de son type
    ElementGraphique* el;
    switch (type_el) {
        case 10: el = new ReservoirGraphique(id); break;
        case 11: el = new EchangeurThmGraphique(id); break;
        case 12: el = new ValveGraphique(id); break;
        case 13: el = new PompeGraphique(id); break;
        case 20: el = new TuyauGrph1x3(id); break;
        case 21: el = new TuyauGrph1x5(id); break;
        case 22: el = new TuyauGrphCourbe2x2(id); break;
        case 23: el = new TuyauGrphCourbe4x4(id); break;
        case 24: el = new TuyauGrph1x1(id); break;
        default: {
            std::cerr << "Element non trouvé" << std::endl;
            assert(0); // Erreur = types_possibles ou ce switch ne sont pas à jour
        }
    }

    // ajoute l'élément à l'EspaceTravail
    element.insert(std::make_pair(id, el));
}





void EspaceTravail::supprimer_element(ElementGraphique *ptr) {
    element.erase(ptr->id);
    delete ptr;
}





void EspaceTravail::sauvegarder(const std::string& nom_fichier) {
    json j;
    j["nom_espace"] = nom;
    j["gaz"] = gaz;
    j["zoom"] = zoom;
    j["decalX"] = decalX;
    j["decalY"] = decalY;

    // ajoute les infos de chaque élément
    j["elements"] = json::array();
    for (auto& el: element) {
        j["elements"].push_back(el.second->get_json());
    }

    // s'assure que le chemin fini par .gzd
    std::string nom_final = nom_fichier;
    if (nom_final.length() < 4) {
        nom_final.append(".gzd");
    }
    if (nom_final.compare(nom_final.length() - 4, 4, ".gzd") != 0) {
        nom_final.append(".gzd");
    }


    // sauvegarde dans un fichier
    std::ofstream output;
    output.open(nom_final, std::ios::out);
    if (output.fail()) {
        throw std::runtime_error("Impossible de sauvegarder dans le fichier (" + nom_final + ")");
    }

    output << j.dump(2);
    output.close();

    this->chemin_fichier = nom_final;
}




Couple EspaceTravail::get_origine() const {
    int vraie_taille_grille = taille_grille * zoom;

    Couple decalage = Couple((decalX + translation_delta_x) * vraie_taille_grille, (decalY + translation_delta_y) * vraie_taille_grille);
    Couple c = get_centre_fenetre(win);
    c.x -= vraie_taille_grille/2 - decalage.x;
    c.y -= vraie_taille_grille/2 - decalage.y;

    return c;
}





Couple EspaceTravail::get_coo_souris_grille() const {
    int mx, my;
    SDL_GetMouseState(&mx, &my);

    int vraie_taille_grille = taille_grille * zoom;

    Couple c = get_origine();

    Couple delta_souris = Couple(mx-c.x, my-c.y);
    if (delta_souris.x < 0) {delta_souris.x -= vraie_taille_grille;}
    if (delta_souris.y < 0) {delta_souris.y -= vraie_taille_grille;}


    Couple pos_grille_finale = Couple(delta_souris.x/vraie_taille_grille, delta_souris.y/vraie_taille_grille);

    return pos_grille_finale;
}




ElementGraphique* EspaceTravail::get_element_survole(const Couple& coo) const {
    Couple c = get_origine();

    for (auto& el: element) {
        SDL_Rect r_el = get_rectangle_element(el.second, c);
        SDL_Point p_coo;
        p_coo.x = coo.x, p_coo.y = coo.y;

        // el est bien survolé par les coordonnées spécifiées
        if (SDL_PointInRect(&p_coo, &r_el)) {
            return el.second;
        }
    }

    return nullptr;
}



void EspaceTravail::afficher_controles_sim(SDL_Window * window) {
    ImGui::SetNextWindowBgAlpha(1.0);
    ImGui::Begin("Contrôles de la simulation", nullptr, MENUS_FENETRE_FLAGS_DEFAUT | ImGuiWindowFlags_AlwaysAutoResize);

    if (sim == nullptr) {
        if (ImGui::Button("S")) {
            try {construire_simulation();}
            catch(std::runtime_error& e) {ouvrir_popup(e.what(), true);}
        }
    }
    else {if (ImGui::Button("R")) {supprimer_simulation();}}
    ImGui::SameLine();
    if (pause) {if (ImGui::Button(">")) {if (sim != nullptr) {pause = false;}}}
    else {if (ImGui::Button("P")) {pause = true;}}

    ImGui::SameLine();
    if (ImGui::Button("Step")) {
        if (sim != nullptr) {
            sim->simuler_etape(modificateur_vitesse);
            temps_ecoule += modificateur_vitesse / 1000.0;
        }
    }

    ImGui::SameLine();
    ImGui::SetNextItemWidth(130);
    ImGui::InputFloat("##vitesse", &modificateur_vitesse, 0.000001, 0.01, "%.6fx");
    if (modificateur_vitesse <= 0) {modificateur_vitesse = 0.000001;}


    // Affiche la durée de simulation déjà écoulée
    if (sim != nullptr) {
        ImGui::Text("Temps écoulé :");
        ImGui::SameLine();
        ImGui::PushItemWidth(77);
        if (temps_ecoule < 0.0001) {
            double temp_us = temps_ecoule * 1000000;
            ImGui::InputDouble("##temps_ecoule", &temp_us, 0.0, 0.0, "%.3fus", ImGuiInputTextFlags_ReadOnly);
        }
        else if (temps_ecoule < 0.1) {
            double temps_ms = temps_ecoule * 1000;
            ImGui::InputDouble("##temps_ecoule", &temps_ms, 0.0, 0.0, "%.3fms", ImGuiInputTextFlags_ReadOnly);
        }
        else {
            ImGui::InputDouble("##temps_ecoule", &temps_ecoule, 0.0, 0.0, "%.3fs", ImGuiInputTextFlags_ReadOnly);
        }
    }


    // positionne la fenêtre en bas au centre de l'écran
    ImVec2 taille_controles = ImGui::GetWindowSize();
    Couple taille_fenetre; SDL_GetWindowSize(window, &taille_fenetre.x, &taille_fenetre.y);
    auto window_pos = ImVec2(taille_fenetre.x / 2 - (taille_controles.x / 2), taille_fenetre.y - taille_controles.y);
    ImGui::SetWindowPos(window_pos);

    ImGui::End();
}




void EspaceTravail::afficher_popup(SDL_Window* window) {
    // Style de la pop-up, en fonction de si c'est une erreur ou non.
    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.0, 0.0, 0.0, 0.0));
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.0, 0.0, 0.0, 0.2));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.0, 0.0, 0.0, 0.3));

    // Rouge si erreur ou verte si réussite.
    if (pop_up_erreur) {ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.539, 0.176, 0.242, 1.0));}
    else {ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.25, 0.637, 0.352, 1.0));}


    ImGui::Begin("popup", nullptr, MENUS_FENETRE_FLAGS_DEFAUT | ImGuiWindowFlags_AlwaysAutoResize);
    ImGui::Text("%s", popup_msg.c_str());
    ImGui::SameLine();
    if (ImGui::Button("x")) {pop_up_affichee = false;}


    // Positionne la fenêtre pop-up correctement
    ImVec2 taille_popup = ImGui::GetWindowSize();
    Couple taille_fenetre; SDL_GetWindowSize(window, &taille_fenetre.x, &taille_fenetre.y);
    auto window_pos = ImVec2(taille_fenetre.x / 2 - (taille_popup.x / 2), taille_fenetre.y - 100);
    ImGui::SetWindowPos(window_pos);


    ImGui::End();

    // suppression du style spécial
    ImGui::PopStyleColor(4);
}





void EspaceTravail::supprimer_simulation() {
    // Supprime les liens vers les éléments de la simulation.
    // Cela remettra automatiquement les fenêtres des éléments en mode "préparation"
    for (auto& el: element) {
        el.second->elt_correspd = nullptr;
    }

    // désalloue le Simulateur
    delete sim;
    sim = nullptr;
}



void EspaceTravail::construire_simulation() {
    if (sim != nullptr) {assert(0);}
    sim = construire_simulateur(*this);
    temps_ecoule = 0.0;
}


void EspaceTravail::ouvrir_popup(const std::string& msg, bool erreur) {
    popup_msg = msg;
    pop_up_affichee = true;
    pop_up_erreur = erreur;
}


void EspaceTravail::afficher_menu(SDL_Window * window) {
    ImGui::SetNextWindowBgAlpha(0.8);
    ImGui::Begin("Menu déroulant", nullptr, MENUS_FENETRE_FLAGS_DEFAUT | ImGuiWindowFlags_MenuBar);
    




    ImGui::SetWindowPos(ImVec2(0, 0));
    Couple taille_fenetre; SDL_GetWindowSize(window, &taille_fenetre.x, &taille_fenetre.y);
    ImGui::SetWindowSize(ImVec2(220, taille_fenetre.y));

    // Indique quelle pop-up est ouverte. Le code est compliqué dû à un bug d'ImGui
    // 0 = aucune, 1 et suivante = pop-up dans l'ordre d'écriture du code.

    if(ImGui::BeginMenuBar()){
        if(ImGui::BeginMenu("Fichier", true)){

            if(ImGui::MenuItem("Nouveau...", NULL, false, true)) {
                menu_popup_a_ouvrir = 1;
            }

            if(ImGui::MenuItem("Ouvrir...", NULL, false, true)) {
                menu_popup_a_ouvrir = 2;
            }

            if(ImGui::MenuItem("Enregistrer", NULL, false, !chemin_fichier.empty())) {
                try {
                    sauvegarder(chemin_fichier);
                    ouvrir_popup("L'espace de travail a bien été enregistré", false);
                }
                catch(std::runtime_error& e) {ouvrir_popup(e.what(), true);}
            }

            if(ImGui::MenuItem("Enregistrer sous...", NULL, false, true)) {
                menu_popup_a_ouvrir = 3;
            }

            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Affichage", true)) {
            if(ImGui::MenuItem("Rétablir zoom", NULL, false, true)){
                zoom = 1.0;
            }
            if(ImGui::MenuItem("Recentrer", NULL, false, true)){
                decalX = 0;
                decalY = 0;
            }
            if(grille_affichee){
                if(ImGui::MenuItem("Cacher grille", NULL, false, true)){
                    grille_affichee = false;
                }
            }else{
                if(ImGui::MenuItem("Afficher grille", NULL, false, true)){
                    grille_affichee = true;
                }
            }
            
            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Aide ?", true)) {
            menu_popup_a_ouvrir = 4;
            ImGui::EndMenu();
        }

        ImGui::EndMenuBar();
    }



    // Pop-up pour "Nouveau"
    if (ImGui::BeginPopupModal("Nouveau", nullptr, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_Popup)) {

        // Centre la pop-up
        ImVec2 taille_controles = ImGui::GetWindowSize();
        Couple taille_fenetre; SDL_GetWindowSize(window, &taille_fenetre.x, &taille_fenetre.y);
        auto window_pos = ImVec2(taille_fenetre.x / 2 - (taille_controles.x / 2), taille_fenetre.y / 2 - (taille_controles.y / 2));
        ImGui::SetWindowPos(window_pos);

        ImGui::Text("Êtes-vous sûr? Toute modification non-sauvegardée sera perdue.");

        if (ImGui::Button("Annuler")) {ImGui::CloseCurrentPopup();}
        ImGui::SameLine();
        if (ImGui::Button("Confirmer")) {
            ImGui::CloseCurrentPopup();
            vider();
        }
        ImGui::EndPopup();
    }


    // Po-up pour "Ouvrir"
    if (ImGui::BeginPopupModal("Ouvrir", nullptr, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_Popup)) {

        // Centre la pop-up
        ImVec2 taille_controles = ImGui::GetWindowSize();
        Couple taille_fenetre; SDL_GetWindowSize(window, &taille_fenetre.x, &taille_fenetre.y);
        auto window_pos = ImVec2(taille_fenetre.x / 2 - (taille_controles.x / 2), taille_fenetre.y / 2 - (taille_controles.y / 2));
        ImGui::SetWindowPos(window_pos);

        ImGui::Dummy(ImVec2(0,5));

        static std::string chemin = chemin_fichier;
        ImGui::InputText("##chemin du fichier", &chemin);

        ImGui::Dummy(ImVec2(0,5));
        ImGui::Separator();
        ImGui::Dummy(ImVec2(0,2));

        ImGui::Text("Toute modification non-sauvegardée sera perdue.");

        ImGui::Dummy(ImVec2(0,3));

        if (ImGui::Button("Annuler")) {ImGui::CloseCurrentPopup();}
        ImGui::SameLine();
        if (ImGui::Button("Confirmer")) {
            ImGui::CloseCurrentPopup();
            try{charger_fichier(chemin);}
            catch(std::runtime_error& e) {ouvrir_popup(e.what(), true);}
        }
        ImGui::EndPopup();
    }



    // Enregistrer sous"
    if (ImGui::BeginPopupModal("Enregistrer sous", nullptr, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_Popup)) {

        // Centre la pop-up
        ImVec2 taille_controles = ImGui::GetWindowSize();
        Couple taille_fenetre; SDL_GetWindowSize(window, &taille_fenetre.x, &taille_fenetre.y);
        auto window_pos = ImVec2(taille_fenetre.x / 2 - (taille_controles.x / 2), taille_fenetre.y / 2 - (taille_controles.y / 2));
        ImGui::SetWindowPos(window_pos);

        static std::string chemin = chemin_fichier;
        ImGui::InputText("##chemin du fichier", &chemin);

        if (ImGui::Button("Annuler")) {ImGui::CloseCurrentPopup();}
        ImGui::SameLine();
        if (ImGui::Button("Enregistrer")) {
            ImGui::CloseCurrentPopup();
            try{
                sauvegarder(chemin);
                ouvrir_popup("L'espace de travail a bien été enregistré sous le nom \"" + chemin + "\"", false);
            }
            catch(std::runtime_error& e) {ouvrir_popup(e.what(), true);}
        }
        ImGui::EndPopup();
    }


    // Pop_up pour "Aide"
    ImGui::SetNextWindowBgAlpha(1.0);
    if (ImGui::BeginPopupModal("Aide", nullptr, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_Popup)) {

        // Centre la pop-up
        ImVec2 taille_controles = ImGui::GetWindowSize();
        Couple taille_fenetre; SDL_GetWindowSize(window, &taille_fenetre.x, &taille_fenetre.y);
        auto window_pos = ImVec2(taille_fenetre.x / 2 - (taille_controles.x / 2), taille_fenetre.y / 2 - (taille_controles.y / 2));
        ImGui::SetWindowPos(window_pos);

        const static int LARGEUR = 700;
        
        ImGui::Text("Ouverture de fichier");
    
        if(ImGui::BeginChild("Ouverture de fichier", ImVec2(LARGEUR,52), true)){
            ImGui::Text("* Pour ouvrir un fichier vide, cliquer sur 'Menu' puis sur 'Nouveau ...'");
            ImGui::Text("* Pour ouvrir un fichier existant, cliquer sur 'Menu' puis sur 'Ouvrir ...'");
        }
        ImGui::EndChild();

        ImGui::Dummy(ImVec2(0,20));



        ImGui::Text("Ajout et suppression de pièces");

        if(ImGui::BeginChild("Ajout et suppression de pièces", ImVec2(LARGEUR,52), true)){
            ImGui::Text("* Cliquer sur une des icônes du menu pour faire apparaître la pièce correspondante au centre de l'écran");
            ImGui::Text("* Pour la supprimer, sélectionner la pièce et la faire glisser dans le menu latéral");
        }
        ImGui::EndChild();

        ImGui::Dummy(ImVec2(0,20));



        ImGui::Text("Gestion des propriétés");

        if(ImGui::BeginChild("Gestion des propriétés", ImVec2(LARGEUR,32), true)){
            ImGui::Text("* Pour modifier les propriétés d'un objet, double-cliquer sur celui-ci");
        }
        ImGui::EndChild();

        ImGui::Dummy(ImVec2(0,20));



        ImGui::Text("Partie Simulation");

        if(ImGui::BeginChild("Partie Simulation", ImVec2(LARGEUR,132), true)){
            ImGui::Text("* Basculer sur l'espace de simulation en cliquant sur 'S' dans le panneau de contrôle en bas de la fenêtre");
            ImGui::Text("* Attention ! Les propriétés des éléments ne sont pas modifiables pendant la simulation sauf");
            ImGui::Text("pour l'ouverture de la valve et le débit de la pompe");
            ImGui::Text("* Pour lancer la simulation cliquer sur '>', puis sur 'P' pour metre en pause");
            ImGui::Text("* Pour augmenter le changement de vitesse de la simulation de manière plus effective,");
            ImGui::Text("maintenir la touche contrôle en même temps que le clic qur les flêches");
        }
        ImGui::EndChild();

        ImGui::Dummy(ImVec2(0,20));



        ImGui::Text("Enregistrements");
        
        if(ImGui::BeginChild("Enregistrements", ImVec2(LARGEUR,52), true)){
            ImGui::Text("* Pour enregistrer un nouveau fichier, cliquer sur 'Menu' puis sur 'Enregistrer Sous ...'");
            ImGui::Text("* Pour enregistrer les modifcations du fichier, cliquer sur 'Menu' puis sur 'Enregistrer'");
        }
        ImGui::EndChild();

        ImGui::Dummy(ImVec2(0,20));


        // Affiche l'easter egg en l'animant en fonction du temps écoulé
        ImGui::Dummy(ImVec2(285, 0));
        ImGui::SameLine();
        unsigned int frame = (SDL_GetTicks() / 750) % 4;
        switch (frame) {
            case 0: ImGui::Image(tex_easter->toboggan_0, {78, 56}); break;
            case 1: ImGui::Image(tex_easter->toboggan_1, {78, 56}); break;
            case 2: ImGui::Image(tex_easter->toboggan_2, {78, 56}); break;
            case 3: ImGui::Image(tex_easter->toboggan_3, {78, 56}); break;
            default: assert(0); // impossible car frame < 4
        }


        ImGui::Dummy(ImVec2(0,20));


        if (ImGui::Button("Fermer")) {ImGui::CloseCurrentPopup();}

        ImGui::EndPopup();
    }





    switch (menu_popup_a_ouvrir) {
        case 1: {
            ImGui::OpenPopup("Nouveau");
            menu_popup_a_ouvrir = 0;
            break;
        }
        case 2: {
            ImGui::OpenPopup("Ouvrir");
            menu_popup_a_ouvrir = 0;
            break;
        }
        case 3: {
            ImGui::OpenPopup("Enregistrer sous");
            menu_popup_a_ouvrir = 0;
            break;
        }
        case 4: {
            ImGui::OpenPopup("Aide");
            menu_popup_a_ouvrir = 0;
            break;
        }
        default: break;
    }


    // Changement du nom de l'espace de travail
    ImGui::Dummy(ImVec2(0,5));
    ImGui::InputText("##nom_espace", &nom);



    ImGui::Dummy(ImVec2(0,5));
    if(ImGui::BeginChild("Gaz", ImVec2(200,60), true)){
        ImGui::Text("Gaz");
        ImGui::PushItemWidth(150);
        const char* gaz_choix[] = {"Hélium", "Dihydrogène", "Dioxygène"};
        if(ImGui::BeginCombo("##gaz",gaz_choix[gaz])) {
            for (int i=0; i < 3; i++) {
                bool est_selectionne = (gaz == i);
                if (ImGui::Selectable(gaz_choix[i], est_selectionne)) {
                    gaz = static_cast<Gaz>(i);
                }

                // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
                if (est_selectionne) {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndCombo();
        }
    }
    ImGui::EndChild();

    ImGui::Dummy(ImVec2(0,5));

    if (ImGui::ImageButton("Réservoir", (ImTextureID) textures->reservoir_text, ImVec2(30*1.5,40*1.5))) {
        try {ajouter_element(10);}
        catch (std::runtime_error& e) { ouvrir_popup(e.what(), true);}
    }

    ImGui::SameLine();
    ImGui::Text("Réservoir");

    ImGui::Dummy(ImVec2(0,5));
    ImGui::Separator();
    ImGui::Dummy(ImVec2(0,5));

    if (ImGui::ImageButton("Pompe",(ImTextureID) textures->pompe_text, ImVec2(30*1.5,30*1.5))) {
        try {ajouter_element(13);}
        catch (std::runtime_error& e) { ouvrir_popup(e.what(), true);}
    }

    ImGui::SameLine();
    ImGui::Text("Pompe");



    ImGui::Dummy(ImVec2(0,5));
    ImGui::Separator();
    ImGui::Dummy(ImVec2(0,5));

    ImGui::Dummy(ImVec2(8, 0)); ImGui::SameLine();

    if (ImGui::ImageButton("Valve",(ImTextureID) textures->valve_text, ImVec2(10*1.5,30*1.5))) {
        try {ajouter_element(12);}
        catch (std::runtime_error& e) { ouvrir_popup(e.what(), true);}
    }

    ImGui::SameLine();
    ImGui::Dummy(ImVec2(8, 0)); ImGui::SameLine();
    ImGui::Text("Valve");



    ImGui::Dummy(ImVec2(0,5));
    ImGui::Separator();
    ImGui::Dummy(ImVec2(0,5));

    if (ImGui::ImageButton("Échangeur Thermique",(ImTextureID) textures->echangeur_text, ImVec2(30*1.5,30*1.5))) {
        try {ajouter_element(11);}
        catch (std::runtime_error& e) { ouvrir_popup(e.what(), true);}
    }

    ImGui::SameLine();
    ImGui::Text("Échangeur Thermique");






    ImGui::Dummy(ImVec2(0,5));
    ImGui::Separator();
    ImGui::Dummy(ImVec2(0,5));

    ImGui::Dummy(ImVec2(8, 0)); ImGui::SameLine();
    if (ImGui::ImageButton("Tuyau 1x1",(ImTextureID) textures->tuyau_1x3_text, ImVec2(10*1.5,30*1.5))) {
        try {ajouter_element(24);}
        catch (std::runtime_error& e) { ouvrir_popup(e.what(), true);}
    }
    ImGui::SameLine();

    ImGui::Dummy(ImVec2(8, 0)); ImGui::SameLine();
    ImGui::Text("Tuyau 1x1");






    ImGui::Dummy(ImVec2(0,5));
    ImGui::Separator();
    ImGui::Dummy(ImVec2(0,5));

    ImGui::Dummy(ImVec2(8, 0)); ImGui::SameLine();
    if (ImGui::ImageButton("Tuyau 1x3",(ImTextureID) textures->tuyau_1x3_text, ImVec2(10*1.5,30*1.5))) {
        try {ajouter_element(20);}
        catch (std::runtime_error& e) { ouvrir_popup(e.what(), true);}
    }
    ImGui::SameLine();

    ImGui::Dummy(ImVec2(8, 0)); ImGui::SameLine();
    ImGui::Text("Tuyau 1x3");



    ImGui::Dummy(ImVec2(0,5));
    ImGui::Separator();
    ImGui::Dummy(ImVec2(0,5));

    ImGui::Dummy(ImVec2(8, 0)); ImGui::SameLine();
    if (ImGui::ImageButton("Tuyau 1x5",(ImTextureID) textures->tuyau_1x3_text, ImVec2(10*1.5,30*1.5))) {
        try {ajouter_element(21);}
        catch (std::runtime_error& e) { ouvrir_popup(e.what(), true);}
    }
    ImGui::SameLine();

    ImGui::Dummy(ImVec2(8, 0)); ImGui::SameLine();
    ImGui::Text("Tuyau 1x5");



    ImGui::Dummy(ImVec2(0,5));
    ImGui::Separator();
    ImGui::Dummy(ImVec2(0,5));

    if (ImGui::ImageButton("Tuyau Coudé 2x2",(ImTextureID) textures->tuyau_2x2_text, ImVec2(30*1.5,30*1.5))) {
        try {ajouter_element(22);}
        catch (std::runtime_error& e) { ouvrir_popup(e.what(), true);}
    }
    ImGui::SameLine();

    ImGui::Text("Tuyau Coudé 2x2");





    ImGui::Dummy(ImVec2(0,5));
    ImGui::Separator();
    ImGui::Dummy(ImVec2(0,5));

    if (ImGui::ImageButton("Tuyau Coudé 4x4",(ImTextureID) textures->tuyau_2x2_text, ImVec2(30*1.5,30*1.5))) {
        try {ajouter_element(23);}
        catch (std::runtime_error& e) { ouvrir_popup(e.what(), true);}
    }
    ImGui::SameLine();

    ImGui::Text("Tuyau Coudé 4x4");


    ImGui::End();
}