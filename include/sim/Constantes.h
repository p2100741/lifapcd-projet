//
// Created by leo on 02/03/23.
//

#ifndef LIFAPCD_PROJET_CONSTANTES_H
#define LIFAPCD_PROJET_CONSTANTES_H

/** @brief Constante universelle des gaz parfaits, en J mol⁻¹ K⁻¹ */
const long double R = 8.314462618;

/** @brief Indice adiabatique d'un gaz monoatomique */
const long double ID_ADDIA_MONO = 5.0/3.0;

/** @brief Indice adiabatique d'un gaz diatomique */
const long double ID_ADDIA_DI = 7.0/5.0;

/** @brief Indice adiabatique d'un gaz triatomique */
const long double ID_ADDIA_TRI = 8.0/6.0;

/** @brief Capacité thermique volumique de l'air, en J m⁻³ K⁻¹ */
const long double CTV_AIR = 1256;


/** @brief Vitesse du son (Mach 1), en m s⁻¹ */
const long double MACH_1 = 343.0;

/** @brief Pression par défaut des éléments, en Pa (pression atmosphérique à 0m) */
const long double PRESSION_DEFAUT = 101325.0;

/** @brief Température par défaut des éléments, en K (20°C)*/
const long double TEMPERATURE_DEFAUT = 293.15;

/** @brief Nombre d'entités qui se trouvent dans une mole de matière (nombre d'Avogadro) */
const long double AVOGADRO = 6.02214E23;

/** @brief Approximation de la racine carrée de 2 */
const long double SQRT_2 = 1.414213562;

/** @brief Constante de Boltzmann, en J K⁻¹ */
const long double BOLTZMANN = 1.380649E-23;

#endif //LIFAPCD_PROJET_CONSTANTES_H
