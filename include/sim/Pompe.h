//
// Created by leo on 02/03/23.
//

#ifndef LIFAPCD_PROJET_POMPE_H
#define LIFAPCD_PROJET_POMPE_H

#include "Element.h"

/**
 * @brief Une pompe est un élément transférant de manière constante une quantité de matière équivalente à un volume fixe
 * d'un élément à un autre.
 */
class Pompe: public Element {
public:
    const static int NB_PORTS = 2;
    const static unsigned int ID_TYPE = 13;

    /**
     * Constructeur par défaut
     * @param debit Le débit de la pompe, en m³ s⁻¹
     */
    Pompe(long double debit);

    /**
     * @brief Défini le débit de la pompe
     * @param debit unité de volume par unité de temps (m³ s⁻¹)
     */
    void set_debit(long double debit);

    /**
     * @brief Renvoie le débit de la pompe
     * @return le débit de la pompe, en m³ s⁻¹
     */
    long double get_debit() const;


    /**
     * @brief Effectue de manière "manuelle" (sans passer par le "flux d'instructions" habituel)
     * le transfert de matière entre sa source et sa destination.
     * @param delta_t la durée de l'échange, en s
     */
    void transfert_manuel(long double delta_t) const;


    long double get_volume(unsigned int port) const final;
    long double get_pression(unsigned int port) const final;
    long double get_temp(unsigned int port) const final;
    long double get_mol(unsigned int port) const final;
    long double get_diametre(unsigned int port) const final;
    unsigned int get_nb_ports() const final;
    void transferer_matiere(long double quantite, long double temp, unsigned int port) final;
    void update_nom() final;
    unsigned int get_id_type() const final;

private:
    long double debit; // en m³ s⁻¹
};


#endif //LIFAPCD_PROJET_POMPE_H
