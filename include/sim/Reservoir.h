//
// Created by leo on 02/03/23.
//

#ifndef LIFAPCD_PROJET_RESERVOIR_H
#define LIFAPCD_PROJET_RESERVOIR_H

#include "Contenant.h"

/**
 * @brief Un réservoir est un contenant avec un seul port. Il peut être immuable, c-à-d qu'il conservera sa pression et
 * sa température tout en continuant de libérer/absorber des particules de gaz. Cela peut être utile pour représenter
 * une source extérieure de gaz, ou une sortie (en définissant sa pression à la pression atmosphérique, par exemple)
 */
class Reservoir: public Contenant {
private:
    bool immuable;

public:
    const static int NB_PORTS = 1;
    const static unsigned int ID_TYPE = 10;

    /**
     * @brief Constructeur par défaut
     * @param volume en m³
     * @param temp en K
     * @param pression en Pa (kg m⁻¹ s⁻²)
     * @param diametre le diamètre de la sortie du réservoir, en m
     * @param immuable si le réservoir conserve ses caractéristiques dans tous les cas, ou non
     */
    Reservoir(long double volume, long double temp, long double pression, long double diametre, bool immuable);

    unsigned int get_nb_ports() const final;
    void transferer_matiere(long double quantite, long double temp, unsigned int port) final;
    void update_nom() final;
    unsigned int get_id_type() const final;
};


#endif //LIFAPCD_PROJET_RESERVOIR_H
