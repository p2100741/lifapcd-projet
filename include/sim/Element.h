//
// Created by leo on 28/02/23.
//

#ifndef LIFAPCD_PROJET_ELEMENT_H
#define LIFAPCD_PROJET_ELEMENT_H

#include "Simulateur.h"
#include <string>
#include <unordered_map>


class Simulateur;
class Element;

/**
 * @brief Un Element A connecté à un Element B stockera cette information sous la forme d'une association clé-valeur:
 * <port de A, Connexion {B&, port de B}>
 */
struct Connexion {
    Element* el;
    unsigned int port;
};


/**
 * @brief Classe de base dont hérite chaque éléments de la simulation (tuyaux, réservoirs, pompes, etc.)
 * Un Element possède au moins un port, représenté par un entier naturel, correspondant à un point de connexion
 * avec un autre Element. Il est utile de différencier les ports pour les Elements dont le comportement change en
 * fonction du port (ex: une pompe avec une entrée et une sortie)
 */
class Element {
public:
    std::string nom;


    virtual ~Element();


    /**
     * @brief Met à jour la donnée membre 'nom' de l'objet.
     * En effet, au cours de la vie d'un Element, son id peut changer, et il faut changer son nom en conséquence
     */
    virtual void update_nom() = 0;

    /**
     * @brief Applique une étape de simulation d'une durée de delta_t s.
     * @param delta_t la durée de l'étape à simuler, en s
     */
    void update(long double delta_t);


    /**
     * @brief Renvoie la vitesse du flux de gaz entre cet élément et l'élément connecté au port donné
     * @param port l'entier indiquant le port prit en compte pour l'interaction
     * @return la vitesse du gaz se déplaçant de cet élément à l'élément connecté, en m s⁻¹
     */
    long double get_vitesse_gaz(unsigned int port) const;

    /** @brief lie l'élément au simulateur */
    void set_simulateur(Simulateur* sim);

    /** @brief défini l'id de l'élément */
    void set_id(unsigned int id);

    /** @brief Ajoute à l'élément une connexion partant de son port port jusqu'au port autre_port de autre_el  */
    void set_connexion(unsigned int port, Element* autre_el, unsigned int autre_port);

    /** @brief Renvoie true si un élément est déjà connecté au port spécifié */
    bool est_connecte(unsigned int port) const;


    /** @brief Renvoie l'entier naturel représentant le type de l'élément.
     * Ex: un objet de type Tuyau renverra 14.
     * @return un entier >= 10 représentant le type de l'élément.
     */
    virtual unsigned int get_id_type() const = 0;


    /**
     * @brief Renvoie le volume du contenant avec lequel interagirait un flux venant du port spécifié.
     * Cette explication un peu complexe est due à l'existence d'éléments qui ne sont pas des contenants (valve, etc.)
     * Une valve, par exemple, renverra le volume du contenant suivant dans la direction de l'interaction.
     * @param port l'entier indiquant le port prit en compte pour l'interaction
     * @return le volume du contenant de l'interaction, en m³
     */
     virtual long double get_volume(unsigned int port) const = 0;

     /**
      * @brief Renvoie la pression du gaz présent dans le contenant avec lequel interagirait un flux venant du port spécifié.
      * Cf. `get_volume` pour + de détails
      * @param port l'entier indiquant le port prit en compte pour l'interaction
      * @return la pression du contenant de l'interaction, en Pa (kg m⁻¹ s⁻²)
      */
     virtual long double get_pression(unsigned int port) const = 0;

     /**
      * @brief Renvoie la température du gaz présent dans le contenant avec lequel interagirait un flux venant du port spécifié.
      * Cf. `get_volume` pour + de détails
      * @param port l'entier indiquant le port prit en compte pour l'interaction
      * @return la température du contenant de l'interaction, en K
      */
     virtual long double get_temp(unsigned int port) const = 0;

     /**
      * @brief Renvoie la quantité de matière du gaz présent dans le contenant avec lequel interagirait un flux venant du port spécifié.
      * Cf. `get_volume` pour + de détails
      * @param port l'entier indiquant le port prit en compte pour l'interaction
      * @return la quantité de matière présente dans le contenant de l'interaction, en mol
      */
     virtual long double get_mol(unsigned int port) const = 0;

     /**
      * @brief Renvoie le nombre de ports que possède cet élément
      * @return le nombre de ports de l'élément
      */
     virtual unsigned int get_nb_ports() const = 0;

     /**
      * @brief Renvoie le diamètre du port spécifié de l'élément
      * @param port l'entier indiquant le port considéré
      * @return le diamètre du port, en m
      */
      virtual long double get_diametre(unsigned int port) const = 0;


     /**
      * @brief Transfert une certaine quantité de matière à cet élément, en passant par le port spécifié.
      * @param quantite le nombre de mol à ajouter/retirer (en fonction du signe) à l'élément (en mol)
      * @param temp la température de la matière a ajouter (si quantite > 0, sinon valeur ignorable)
      * @param port Le port par lequel s'effectue le transfert
      */
     virtual void transferer_matiere(long double quantite, long double temp, unsigned int port) = 0;



protected:
    Simulateur* simulateur;
    unsigned int id_element;                                // id de l'élément dans le simulateur
    std::unordered_map<unsigned int, Connexion> connexions;
};


#endif //LIFAPCD_PROJET_ELEMENT_H
