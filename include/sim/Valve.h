//
// Created by leo on 02/03/23.
//

#ifndef LIFAPCD_PROJET_VALVE_H
#define LIFAPCD_PROJET_VALVE_H

#include "Element.h"

/**
 * @brief Une valve est un élément permettant de contrôler le diamètre d'une connexion entre deux éléments.
 * Celle aura une influence sur le débit de gaz entre les deux éléments.
 */
class Valve: public Element {
private:
    long double ouverture;              // de 0 à 1 (taux d'ouverture de la valve)

public:
    const static int NB_PORTS = 2;
    const static unsigned int ID_TYPE = 12;

    /**
     * @brief Constructeur par défaut
     */
    Valve(long double ouverture);

    /**
     * @brief Défini le taux d'ouverture de la valve
     * @param ouverture un long double entre O et 1
     */
    void set_ouverture(long double ouverture);

    /**
     * @brief Renvoie l'ouverture actuelle de la valve
     * @return l'ouverture actuelle de la valve
     */
    long double get_ouverture() const;


    long double get_volume(unsigned int port) const final;
    long double get_pression(unsigned int port) const final;
    long double get_temp(unsigned int port) const final;
    long double get_mol(unsigned int port) const final;
    long double get_diametre(unsigned int port) const final;
    unsigned int get_nb_ports() const final;
    void transferer_matiere(long double quantite, long double temp, unsigned int port) final;
    void update_nom() final;
    unsigned int get_id_type() const final;
};


#endif //LIFAPCD_PROJET_VALVE_H
