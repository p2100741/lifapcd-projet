//
// Created by leo on 28/02/23.
//

#ifndef LIFAPCD_PROJET_SIMULATEUR_H
#define LIFAPCD_PROJET_SIMULATEUR_H

#include "Element.h"
#include <unordered_map>
#include <string>


/**
 * Interval interne maximum de simulation, en ms. Appeler simuler_etape avec une valeur
 * supérieure à cela va simplement appeler simuler_etape
 * plusieurs fois, avec au maximum cette valeur comme interval
 */
const long double MAX_STEP = 5e-1;


/**
 * Nombre de chiffres dans l'identifiant d'un objet servant à l'identifier parmi les autres éléments de ce type.
 * Naturellement, cela implique que le nombre maximum d'élément du même type est 10^NB_CHIFFRES_ID.
 * Pour se simplifier la tâche, on limitera le nombre d'objets total à 10^NB_CHIFFRES_ID.
 */
const unsigned int NB_CHIFFRES_ID = 2;


enum Gaz {
    HELIUM,
    DIHYDROGENE,
    DIOXYGENE,
    //VAPEUR_EAU
};

enum Materiau {
    CUIVRE,
    ACIER,
    VERRE
};




// pré-définition de Element pour éviter un include cyclique
class Element;



/**
 * @brief Classe principale qui englobe et orchestre la simulation d'un système.
 * Une étape de simulation équivaut à appeler la fonction `update` pour chaque élément de la simulation.
 */
class Simulateur {
public:
    Gaz gaz;

    /**
     * @brief Constructeur par défaut
     * @param gaz le gaz utilisé par le système
     */
    explicit Simulateur(Gaz gaz);

    /**
     * @brief Destructeur
     */
    ~Simulateur();

    /**
     * @brief Avance la simulation de `delta_t` ms.
     * @note Cette fonction est récursive, mais uniquement de profondeur 1. En effet, pour toute valeur de delta_t
     * supérieure à MAX_STEP, la fonction va s'appeler elle-même delta_t / MAX_STEP fois de manière itérative.
     * @param delta_t la durée de l'étape à simuler, en s
     */
    void simuler_etape(long double delta_t);

    /**
     * @brief Renvoie la capacité thermique massique du gaz à volume constant
     * @param gaz le gaz
     * @return la capacité thermique massique du gaz, en J K⁻¹ kg⁻¹
     */
    static long double get_ctm(Gaz gaz);

    /**
     * @brief Renvoie la conductivité thermique du matériau
     * @param mat le matériau
     * @return la conductivité thermique du matériau, en W m⁻¹ K⁻¹ (équivalent à kg m s⁻³ K⁻¹)
     */
    static long double get_ct(Materiau mat);

    /**
     * @brief Renvoie la masse molaire du gaz
     * @param gaz le gaz
     * @return la masse molaire du gaz, en kg mol⁻¹
     */
    static long double get_masse_molaire(Gaz gaz);


    /**
     * @brief Renvoie la masse volumique du matériau
     * @param mat le matériau
     * @return la masse volumique du matériau, en kg m⁻³
     */
    static long double get_masse_volumique(Materiau mat);


    /**
     * @brief Renvoie la capacité thermique massique du matériau
     * @param mat le matériau
     * @return la capacité thermique massique du matériau, en J kg⁻¹ K⁻¹
     */
    static long double get_capa_thermique(Materiau mat);


    /**
     * @brief Renvoie la masse moléculaire du gaz
     * @param gaz le gaz
     * @return la masse moléculaire du gaz, en kg mol⁻¹
     */
    static long double get_masse_moleculaire(Gaz gaz);

    /**
     * @brief Renvoie le diamètre d'une particule du gaz
     * Cette valeur est utilisée pour calculer le _libre parcours moyen_ du gaz, qui permet de calculer
     * la conductivité thermique d'un gaz
     * @param gaz le gaz
     * @return le diamètre de sa particule, en m
     */
    static long double get_diametre(Gaz gaz);

    /**
     * @brief Renvoie l'indice adiabatique du gaz (cf. https://fr.wikipedia.org/wiki/Indice_adiabatique)
     * @param gaz le gaz
     * @return l'indice adiabatique du gaz, sans unité
     */
     static long double get_indice_addia(Gaz gaz);

    /**
     * @brief Ajoute un élément au système
     * @param el un pointeur vers un Element stocké sur le tas
     * @param id L'identifiant à donner à l'élément.Si rien n'est spécifié, la fonction en génère un automatiquement.
     * @return l'adresse de l'élément ajouté
     */
     Element* ajouter_element(Element* el, unsigned int id = 0);

     /**
      * @brief Lie les deux éléments entre eux, via les ports spécifiés.
      * @param id_el_1 l'identifiant du premier élément à lier
      * @param port_el_1 le port du premier élément sur lequel la connexion doit être faite
      * @param id_el_2 l'identifiant du deuxième élément à lier
      * @param port_el_2 le port du deuxième élément sur lequel la connexion doit être faite
      */
      void lier_elements(unsigned int id_el_1, unsigned int port_el_1, unsigned int id_el_2, unsigned int port_el_2);

      /**
       * @brief Lie les deux éléments entre eux, via les ports spécifiés.
       * @param ptr_el_1 un pointeur vers l'élément 1
       * @param port_el_1 le port du premier élément sur lequel la connexion doit être faite
       * @param ptr_el_1 un pointeur vers l'élément 2
       * @param port_el_2 le port du deuxième élément sur lequel la connexion doit être faite
       */
      void lier_elements(Element* ptr_el_1, unsigned int port_el_1, Element* ptr_el_2, unsigned int port_el_2);

      /**
       * @brief Renvoie une référence vers l'élément à l'id spécifié
       * @param id l'identifiant de l'élément
       * @return une référence vers l'élément
       */
       Element& get_element(unsigned int id);

       /**
        * @brief Simule l'évolution du système en stockant les valeurs caractéristiques de chaque éléments,
        * puis exporte ces valeurs dans un fichier CSV importable dans un tableur
        * @param nom_fichier le nom du fichier à générer
        * @param duree la durée de simulation à générer, en s
        * @param duree_etape l'interval entre chaque échantillon, en s
        * @param ecrase si un fichier existe déjà avec le nom spécifié, la fonction l'écrase uniquement si ecrase est vrai
        */
       void generer_csv(const std::string& nom_fichier, long double duree, long double interval, bool ecrase = false);

private:
    std::unordered_map<unsigned int, Element*> elements;
};


#endif //LIFAPCD_PROJET_SIMULATEUR_H
