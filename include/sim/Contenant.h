//
// Created by leo on 02/03/23.
//

#ifndef LIFAPCD_PROJET_CONTENANT_H
#define LIFAPCD_PROJET_CONTENANT_H

#include "Element.h"


/**
 * @brief Classe de base pour les Element ayant un volume capable de contenir du gaz
 * En effet, certains éléments ne "contiennent" rien (aux yeux du simulateur), tels que les valves ou les pompes
 */
class Contenant: public Element {
public:
    long double diametre;      // diamètre des ports du Contenant

    ~Contenant() override = default;

    long double get_volume(unsigned int port) const final;
    long double get_pression(unsigned int port) const final;
    long double get_temp(unsigned int port) const final;
    long double get_mol(unsigned int port) const final;
    long double get_diametre(unsigned int port) const final;

    unsigned int get_nb_ports() const override = 0;
    void transferer_matiere(long double quantite, long double temp, unsigned int port) override = 0;
    void update_nom() override = 0;
    unsigned int get_id_type() const override = 0;

protected:
    long double volume;
    long double quantite_mat;
    long double temperature;

    /**
     * @brief Renvoie la nouvelle température du gaz présent dans le contenant lorsque l'on y ajoute cq mol de matière.
     * @param cq la quantité de matière ajoutée, en mol (négatif = on en enlève)
     * @param ajout_temp la température du gaz ajouté. Cette valeur n'est pas utile si du gaz est enlevé (= cq < 0)
     * @return la nouvelle température du gaz, en K
     */
    long double get_nouvelle_temp(long double cq, long double ajout_temp) const;
};


#endif //LIFAPCD_PROJET_CONTENANT_H
