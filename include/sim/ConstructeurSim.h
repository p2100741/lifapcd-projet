//
// Created by leo on 15/03/23.
//

#ifndef LIFAPCD_PROJET_CONSTRUCTEURSIM_H
#define LIFAPCD_PROJET_CONSTRUCTEURSIM_H


#include "../interface/EspaceTravail.h"
#include "Simulateur.h"


/**
 * @brief Combine les coordonnées, la rotation, le pointeur de l'élément et son numéro de port lié à un port
 */
struct InfoPort {
    ElementGraphique* el;
    unsigned int id_port;
    Couple coo;
    unsigned int rota;

    InfoPort(ElementGraphique* element, unsigned int p, Couple c, unsigned int r): el(element), id_port(p), coo(c), rota(r) {}
};



/**
 * @brief Représente un lien entre deux ElementGraphique.
 */
struct ConnexionEspaceTravail {
    ElementGraphique* el1;
    ElementGraphique* el2;
    unsigned int port_el_1;
    unsigned int port_el_2;

    ConnexionEspaceTravail(ElementGraphique* e1, ElementGraphique* e2, unsigned int p1, unsigned int p2): el1(e1), el2(e2), port_el_1(p1), port_el_2(p2) {}
};



/**
 * @brief Construit, alloue et lie un Simulateur à partir d'un espace de travail.
 * Le constructeur va isoler les systèmes fermés dans l'espace de travail, et ignorer tous les éléments n'en
 * faisant pas partie. Il va également lié à chaque ElementGraphique son Element correspondant (en modifiant
 * son pointeur elt_correspd, ou mettre ce pointeur à null si il n'est pas simulé.
 * @param[in,out] espace_travail Une référence vers l'espace de travail duquel générer une simulation.
 * @return Le simulateur ainsi créé
 */
 Simulateur* construire_simulateur(EspaceTravail& espace_travail);


#endif //LIFAPCD_PROJET_CONSTRUCTEURSIM_H
