//
// Created by leo on 02/03/23.
//

#ifndef LIFAPCD_PROJET_TUYAU_H
#define LIFAPCD_PROJET_TUYAU_H

#include "Contenant.h"

/**
 * @brief Le tuyau est le plus basique des Contenant: il est passif et a deux ports.
 */
class Tuyau: public Contenant {
private:
    long double longueur;

public:
    const static int NB_PORTS = 2;
    const static unsigned int ID_TYPE = 14;

    /**
     * @brief Constructeur par défaut.
     */
    Tuyau(long double diametre, long double longueur, long double pression, long double temp);


    /**
     * @brief Renvoie la longueur du tuyau
     * @return la longueur du tuyau, en m
     */
    long double get_longueur() const;


    unsigned int get_nb_ports() const final;
    void transferer_matiere(long double quantite, long double temp, unsigned int port) final;
    void update_nom() final;
    unsigned int get_id_type() const final;
};


#endif //LIFAPCD_PROJET_TUYAU_H
