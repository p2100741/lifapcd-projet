//
// Created by leo on 02/03/23.
//

#ifndef LIFAPCD_PROJET_ECHANGEURTHERMIQUE_H
#define LIFAPCD_PROJET_ECHANGEURTHERMIQUE_H

#include "Contenant.h"
#include "Simulateur.h" // pour Materiau


/**
 * @brief Représente un volume d'air, à pression atmosphérique, avec lequel l'échangeur thermique interagit.
 * Ce volume peut être immuable ou non, de la même manière que les Reservoir.
 * @see Reservoir
 */
struct Espace {
    long double volume;      // en m³
    long double temperature; // en K
    bool immuable;
};


/**
 * @brief un EchangeurThermique, comme son nom l'indique, permet au gaz qu'il contient de transférer/recevoir de
 * l'énergie thermique depuis un espace extérieur au système.
 */
class EchangeurThermique: public Contenant {
private:
    long double surface_interne; // en m²
    long double surface_externe; // en m²
    long double temp_metal;      // en K
    long double volume_tuyau;    // en m³

    Materiau materiau;
    long double epaisseur;       // en m
    Espace espace;


public:
    const static int NB_PORTS = 2;
    const static unsigned int ID_TYPE = 11;

    /**
     * @brief Constructeur par défaut
     * @param espace l'Espace avec lequel cet échangeur interagit
     * @param diametre le diamètre intérieur des tuyaux constituant cet échangeur, en m
     * @param longueur la longueur totale des tuyaux de cet échangeur, en m
     * @param epaisseur l'épaisseur des tuyaux de l'échangeur, en m
     * @param materiau le matériau constituant les tuyaux de cet échangeur
     */
    EchangeurThermique(Espace espace, long double diametre, long double longueur, long double epaisseur, long double temperature, long double pression, Materiau materiau);


    ~EchangeurThermique() override = default;

    /**
     * @brief Renvoie la conductivité thermique du gaz à l'intérieur de l'échangeur
     * @return la conductivité thermique du gaz, en W m⁻¹ K⁻¹ (équivalent à kg m s⁻³ K⁻¹)
     */
    long double get_gaz_ct() const;

    /**
     * @brief Renvoie la valeur du flux thermique s'effectuant entre cet le gaz et le métal constituant
     * cet échangeur
     * @return la valeur du flux thermique, en W (équivalent à kg m² s⁻³).
     * Note: 1W = 1 J s⁻¹
     */
    long double get_ft_gaz_metal() const;

    /**
     * @brief Renvoie la valeur du flux thermique s'effectuant entre cet le métal constituant cet échangeur
     * et l'espace dans lequel se trouve l'échangeur
     * @return la valeur du flux thermique, en W (équivalent à kg m² s⁻³)
     */
     long double get_ft_metal_air() const;

    /**
     * @brief Renvoie la température de l'espace
     * @return la température de l'espace, en K
     */
    long double get_temp_espace() const;


    /**
     * @brief Renvoie la température des tuyaux
     * @return la température des tuyaux, en K
     */
    long double get_temp_tuyau() const;


    /**
     * @brief Renvoie la masse du tuyau constituant cet échangeur
     * @return la masse du tuyau, en kg
     */
    long double get_masse_tuyau() const;

     /**
     * @brief Applique l'échange thermique entre le gaz présent dans l'échangeur et l'espace extérieur.
     * @param delta_t la durée de l'échange, en s
     */
     void echange_thermique(long double delta_t);

     unsigned int get_nb_ports() const final;
     void transferer_matiere(long double quantite, long double temp, unsigned int port) final;
     void update_nom() final;
     unsigned int get_id_type() const final;
};


#endif //LIFAPCD_PROJET_ECHANGEURTHERMIQUE_H
