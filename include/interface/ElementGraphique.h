#ifndef ELEMENT_GRAPHIQUE_H
#define ELEMENT_GRAPHIQUE_H

#include "Couple.h"
#include "../sim/Element.h"

#include <vector>

#include <SDL2/SDL.h>

#include "../../external/json.hpp"

#define IMGUI_DEFINE_MATH_OPERATORS // actives les opérateurs d'ImGui
#include "../../external/imgui.h"


using json = nlohmann::json;

/** @brief Classe dont héritent les objets de simulation (tuyaux, réservoirs, pompes, ...)
* Chaque élément possède une dimension constante sur la grille et des coodonnées pour ses ports de connexion
* Il est contient également l'information d'orientation de ses ports
* Tous les élements graphiques sont rassemblés dans l'espace de travail
*/

class ElementGraphique {
    public:

        const static int FLAGS_FENETRE_DEFAUT = ImGuiWindowFlags_NoCollapse
                    | ImGuiWindowFlags_NoResize
                    | ImGuiWindowFlags_AlwaysAutoResize
                    | ImGuiWindowFlags_NoSavedSettings;;

        Couple pos;
        unsigned int rota;
        unsigned int id;
        bool fenetre_affichee;
        Element * elt_correspd;

        virtual ~ElementGraphique(); // pas besoin de constructeur car classe abstraite

        /** @brief Récupère les coordonnées utilisées pour dimensionner l'objet
        * Elles sont contenues dans sa définition
        */
        virtual const Couple& get_dim() const = 0;

        /** @brief Récupération des coordonnées des ports de l'objet
        * C'est un vecteur car l'objet peut avoir plusieurs ports donc plusieurs couples de coordonnées
        */
        virtual const std::vector<Couple>& get_coo_ports() const = 0;

        /** @brief Renvoie l'identifiant d'orientation de chaque port de l'objet dans un vecteur */
        virtual const std::vector<unsigned int>& get_orientation_ports() const = 0;

        /**
         * @brief Affiche la fenêtre imgui rattachée à cet élément
         * @param renderer le SDL_Renderer à utiliser
         * @param rect Le rectangle d'affichage de l'élément. Il est calculé par EspaceTravail, et est utilisé pour
         * afficher le trait entre la fenêtre et son élément.
         */
        virtual void afficher_fenetre(SDL_Renderer* renderer, SDL_Rect& rect) = 0;

        /** @brief Renvoie les données de l'élément sous forme d'objet json */
        virtual json get_json() const = 0;

        /**
         * @brief Renvoie un entier correspondant au type de l'élément.
         * Cet entier est le même qui identifie le type de l'élément dans son identifiant unique.
         * (ex: 10 = réservoir, 14 = tuyau, etc.)
         */
         virtual unsigned int get_id_type() const = 0;


protected:
    const static unsigned int ROTA_DEFAUT;
    const static Couple POSITION_DEFAUT;
};

#endif