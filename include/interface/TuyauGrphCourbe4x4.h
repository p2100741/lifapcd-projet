//
// Created by leo on 21/04/23.
//

#ifndef LIFAPCD_PROJET_TUYAUGRPH4X4_H
#define LIFAPCD_PROJET_TUYAUGRPH4X4_H


#include "TuyauGraphique.h"

/** @brief Classe héritant de TuyauGraphique et représentant un tuyau droit de 5 cases de long. */
class TuyauGrphCourbe4x4: public TuyauGraphique {
public:
    // Ces constructeurs sont hérités de TuyauGraphique
    TuyauGrphCourbe4x4(unsigned int id);
    TuyauGrphCourbe4x4(long double diametre, long double longueur, long double pression, long double temp,
                       Couple pos, unsigned int rota, unsigned int id);
    TuyauGrphCourbe4x4(const json& j);

    const Couple& get_dim() const final;
    const std::vector<Couple>& get_coo_ports() const final;
    const std::vector<unsigned int>& get_orientation_ports() const final;
    unsigned int get_id_type() const final;



private:
    const static Couple DIMENSIONS; // la taille d'un objet ne change pas
    const static std::vector<Couple> COO_PORTS;
    const static std::vector<unsigned int> ORIENT_PORTS;

    const static unsigned int TYPE_ID = 23;
};


#endif //LIFAPCD_PROJET_TUYAUGRPH4X4_H
