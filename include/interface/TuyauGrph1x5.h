//
// Created by leo on 18/04/23.
//

#ifndef LIFAPCD_PROJET_TUYAUGRPH1X5_H
#define LIFAPCD_PROJET_TUYAUGRPH1X5_H


#include "TuyauGraphique.h"

/** @brief Classe héritant de TuyauGraphique et représentant un tuyau droit de 5 cases de long. */
class TuyauGrph1x5: public TuyauGraphique {
public:
    // Ces constructeurs sont hérités de TuyauGraphique
    TuyauGrph1x5(unsigned int id);
    TuyauGrph1x5(long double diametre, long double longueur, long double pression, long double temp,
                       Couple pos, unsigned int rota, unsigned int id);
    TuyauGrph1x5(const json& j);

    const Couple& get_dim() const final;
    const std::vector<Couple>& get_coo_ports() const final;
    const std::vector<unsigned int>& get_orientation_ports() const final;
    unsigned int get_id_type() const final;



private:
    const static Couple DIMENSIONS; // la taille d'un objet ne change pas
    const static std::vector<Couple> COO_PORTS;
    const static std::vector<unsigned int> ORIENT_PORTS;

    const static unsigned int TYPE_ID = 21;
};


#endif //LIFAPCD_PROJET_TUYAUGRPH1X5_H
