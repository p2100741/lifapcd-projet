#ifndef RESERVOIR_GRAPHIQUE_H
#define RESERVOIR_GRAPHIQUE_H
#include "ElementGraphique.h"


/** @brief Le réservoir graphique fait partie des éléments qui implémentent les fonctions de la classe ElementGraphique */

class ReservoirGraphique: public ElementGraphique { //héritage

    public:
    
        long double volume;
        long double temp;
        long double pression;
        long double diametre;
        bool immuable;

        /** Créé un ReservoirGraphique avec des paramètres par défaut */
        ReservoirGraphique(unsigned int id);


        ReservoirGraphique(long double volume, long double temp, long double pression,
                           long double diametre, bool immuable, Couple pos, unsigned int rota, unsigned int id);

        /** Créé un ReservoirGraphique à partir d'un objet json le décrivant */
        ReservoirGraphique(const json& j);

        ~ReservoirGraphique() = default; // le compilateur se charge de l'écrire par défaut

        const Couple& get_dim() const final; // "final" indique au constructeur "c'est à cette classe que je vais l'implémenter"

        const std::vector<Couple>& get_coo_ports() const final;

        const std::vector<unsigned int>& get_orientation_ports() const final;

        void afficher_fenetre(SDL_Renderer* renderer, SDL_Rect& rect) final;

        unsigned int get_id_type() const final;

        json get_json() const final;

    private:
        const static Couple DIMENSIONS; // la taille d'un objet ne change pas
        const static std::vector<Couple> COO_PORTS;
        const static std::vector<unsigned int> ORIENT_PORTS;
};

#endif