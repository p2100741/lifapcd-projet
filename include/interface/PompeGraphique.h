#ifndef POMPE_GRAPHIQUE_H
#define POMPE_GRAPHIQUE_H
#include "ElementGraphique.h"

#include "../sim/Pompe.h"


/** @brief La pompe fait partie des éléments qui implémentent les fonctions de la classe ElementGraphique */

class PompeGraphique: public ElementGraphique { //héritage

    public:
        long double debit;

        /** Créé une PompeGraphique avec des paramètres par défaut */
        PompeGraphique(unsigned int id);

        PompeGraphique(long double debit, Couple pos, unsigned int rota, unsigned int id);

        /** Créé une PompeGraphique à partir d'un objet json la décrivant */
        PompeGraphique(const json& j);

        ~PompeGraphique() = default;

        const Couple& get_dim() const final;

        const std::vector<Couple>& get_coo_ports() const final;

        const std::vector<unsigned int>& get_orientation_ports() const final;

        void afficher_fenetre(SDL_Renderer* renderer, SDL_Rect& rect) final;

        unsigned int get_id_type() const final;

        json get_json() const final;

    private:
        const static Couple DIMENSIONS; // la taille d'un objet ne change pas
        const static std::vector<Couple> COO_PORTS;
        const static std::vector<unsigned int> ORIENT_PORTS;
};

#endif