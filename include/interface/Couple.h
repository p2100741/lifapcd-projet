#ifndef COUPLE_H
#define COUPLE_H

#include <cmath>
#include <iostream>

/** @brief Vecteur 2D d'entiers, peut représenter notamment une position sur la grille */
struct Couple {
    int x,y;
    Couple(): x(0), y(0) {}
    Couple(int xx, int yy): x(xx), y(yy) {}
    Couple(const Couple& c) = default;


    /**
     * @brief Renvoie le Couple résultant de la rotation de ce Couple par rotation
     * @param rotation un entier (de 0 à 3) représentant l'angle de rotation, par pas de 90° sens anti-horaire.
     * @return le nouveau couple résultant de cette rotation
     */
    Couple rotation(unsigned int rotation) const {
        // pour une raison qui m'échappe, la rotation s'applique dans le sens horaire, il faut donc inverser
        // rotation (sinon, l'axe x est inversé).
        int rota_inversee = (4 - rotation) % 4;

        return Couple (
            std::round(x * cos(rota_inversee * M_PI / 2) - y * sin(rota_inversee * M_PI / 2)),
            std::round(x * sin(rota_inversee * M_PI / 2) + y * cos(rota_inversee * M_PI / 2))
        );
    }
};

inline bool operator==(Couple c1, Couple c2) {
    return c1.x == c2.x && c1.y == c2.y;
}

inline Couple operator+(Couple c1, Couple c2) {
    return{c1.x + c2.x, c1.y + c2.y};
}

inline Couple operator-(Couple c1, Couple c2) {
    return{c1.x - c2.x, c1.y - c2.y};
}

inline Couple operator/(Couple c, int i) {
    return {c.x / i, c.y / i};
}


#endif