#ifndef ESPACE_TRAVAIL_H
#define ESPACE_TRAVAIL_H
#include <string>
#include <unordered_map>
#include <SDL2/SDL.h>
#include "ElementGraphique.h"
#include "../sim/Simulateur.h"
#include <cassert>
#include <SDL2/SDL_ttf.h>


/** @brief Structure stockant les pointeurs vers les textures des ElementGraphique */
struct TexturesElements {
    SDL_Texture * reservoir_text;
    SDL_Texture * valve_text;
    SDL_Texture * pompe_text;
    SDL_Texture * echangeur_text;

    SDL_Texture * tuyau_1x1_text;
    SDL_Texture * tuyau_1x3_text;
    SDL_Texture * tuyau_1x5_text;
    SDL_Texture * tuyau_2x2_text;
    SDL_Texture * tuyau_4x4_text;
};


/** @brief Structure stockant les textures de l'easter-egg */
struct TexturesEaster {
    SDL_Texture * toboggan_0;
    SDL_Texture * toboggan_1;
    SDL_Texture * toboggan_2;
    SDL_Texture * toboggan_3;
};


/** @brief Classe "orchestre" gérant de nombreuses fonctionnalités liées à l'interface graphique. Entre autre:
 * - Affichage de la grille, des éléments, et de toutes les fenêtres ImGui
 * - Gestion des sauvegarde, chargement/déchargement, création de nouveaux fichiers, etc.
 * - Gestion de la simulation temps-réel, de la variation vitesse de simulation
 */
class EspaceTravail {

    public:

        std::string nom; // nom de l'espace de travail

        unsigned int gaz;

        bool grille_affichee;

        std::unordered_map<unsigned int, ElementGraphique *> element;

        TexturesElements* textures;
        TTF_Font* font;

        EspaceTravail(TexturesElements* tex, TTF_Font* font, TexturesEaster* tex_easter);

        float zoom;
        float decalX, decalY;

        /** @brief Constructeur à partir d'un fichier de l'espace de travail
        * Il indique le fichier dans lequel est contenu l'espace de travail
        * @param nom_fichier le nom du fichier source
        */
        explicit EspaceTravail(const std::string& nom_fichier, TexturesElements* tex, TTF_Font* font, TexturesEaster* tex_easter);

        ~EspaceTravail();

        /** @brief Renvoie une référence vers un élément graphique identifié
         * @param id l'identifiant de l'élément
         * @return une référence vers l'élément graphique
         */
        ElementGraphique& get_element(unsigned int id) const;

        /** @brief Réagit à l'événement SDL */
        void evenement(const SDL_Event& event);

        /**
         * @brief Met à jour l'espace de travail
         * @param window La fenêtre SDL
         * @param delta_t le nombre de millisecondes écoulées depuis la dernière frame
         */
        void update(SDL_Window* window, Uint64 delta_t);

        /** @brief Affiche la fenêtre d'espace de travail avec imgui */
        void afficher(SDL_Renderer* renderer, SDL_Window* window);

        /** @brief Sauvergarde les changements de l'espace de travail, en écrasant le fichier s'il existe déjà */
        void sauvegarder(const std::string& nom_fichier);

        /**
         * @brief Ouvre une pop-up ImGui avec le message spécifié.
         * @param msg Le texte à afficher dans la pop-up.
         * @param erreur Si true, la pop-up sera affichée en rouge (signalant une erreur).
         * Sinon, elle sera verte (signalant une réussite).
         */
        void ouvrir_popup(const std::string& msg, bool erreur);

    private:
        const static unsigned int taille_grille = 75;

        const static int MENUS_FENETRE_FLAGS_DEFAUT = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove;

        // chemin vers le fichier ouvert
        std::string chemin_fichier;

        // Textures de l'easter egg
        TexturesEaster* tex_easter;

        // variables utilisées pour les interactions avec l'espace de travail
        bool translation = false;
        float translation_origin_x = 0, translation_origin_y = 0;
        float translation_delta_x = 0, translation_delta_y = 0;

        float modificateur_vitesse = 0.01;
        bool pause = true;
        double temps_ecoule = 0.0;

        Couple delta_el_attrape = Couple(0, 0);
        Couple attrape_coo_souris = Couple(0,0);
        ElementGraphique* ptr_el_attrape = nullptr;

        std::string popup_msg;
        bool pop_up_affichee = false;
        bool pop_up_erreur = false;

        int menu_popup_a_ouvrir = 0;

        // Fenêtre SDL
        SDL_Window* win;

        // Pointeur vers le simulateur, peut être null si aucun n'a été construit
        Simulateur* sim;

        /** @brief Charge le fichier dans l'espace travail */
        void charger_fichier(const std::string& nom_fichier);

        /** @brief Vide l'espace travail actuel */
        void vider();

        /** @brief Désalloue le simulateur, supprime les liens vers les élements pour tous les ElementGraphique */
        void supprimer_simulation();

        /** @brief Construit un simulateur, créé les liens entre les éléments et leur équivalent graphiques */
        void construire_simulation();

        /** @brief Ajoute un ElementGraphique "par défaut" du type spécifié. Peut renvoyer une exception std::runtime_error
         * si l'ajout n'est pas possible (ex: trop d'éléments dans la simulation).
         * @param type_el L'entier identifiant l'ElementGraphique. Ex: 10 = Réservoir, 20 = Tuyau 1x3, etc.
         */
        void ajouter_element(unsigned int type_el);


        /**
         * @brief Supprime l'élément de l'EspaceTravail.
         * Note: la fonction désalloue la valeur indiquée par le pointeur, sans vérifier qu'il s'agit d'un élément de
         * l'Espace.
         * @param ptr Un pointeur vers l'élément à supprimer.
         */
        void supprimer_element(ElementGraphique* ptr);

        /** @brief Renvoie les coordonnées de la souris dans la grille */
        Couple get_coo_souris_grille() const;

        /** @brief Dessine la grille dans le renderer */
        void dessiner_grille(SDL_Renderer* renderer, Couple c) const;

        /** @brief Dessine l'élément spécifié dans le renderer */
        void dessiner_element(SDL_Renderer* renderer, Couple c, ElementGraphique* el) const;

        /** @brief Dessine la fenêtre ImGui de contrôle de la simulation */
        void afficher_controles_sim(SDL_Window* window);

        /**
         * @brief Affiche la fenêtre pop-up. Note: nous n'utilisons pas les pop-up natives de ImGui.
         * @param window La fenêtre SDL
         * @param erreur Si true, la pop-up sera affichée en rouge (signalant une erreur), sinon en vert (signalant
         * une réussite)
         */
        void afficher_popup(SDL_Window* window);

        /** @brief Affiche la barre de menu */
        void afficher_menu(SDL_Window * window);

        /** @brief Renvoie la texture correspondant à l'élément spécifié */
        SDL_Texture* get_texture(const ElementGraphique* el) const;

        /** Renvoie le rectangle dans lequel se situe l'élément donné. Cela est utile pour détecter un clic dessus,
         * par exemple
         * @param el pointeur vers l'élément
         * @param c les coordonnées du centre du repère
         * @return son rectangle englobant
         */
        SDL_Rect get_rectangle_element(const ElementGraphique* el, Couple c) const;

        /** @brief Renvoie l'origine de la grille en coordonnées globales */
        Couple get_origine() const;

        /** @brief Renvoie un pointeur vers l'élément survolé par les coordonnées spécifiées.
         * @param coo les coordonnées (globales, par ex. de la souris)
         * @param window la fenêtre
         * @return un pointeur vers un élément se trouvant sous les coordonnées données, ou nullptr si il n'y en a pas
         */
        ElementGraphique* get_element_survole(const Couple& coo) const;
};

#endif // ESPACE_TRAVAIL_H