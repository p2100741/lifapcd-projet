#ifndef TUYAU_GRAPHIQUE_H
#define TUYAU_GRAPHIQUE_H
#include "ElementGraphique.h"


/** @brief Classe abstraite servant de base pour tous les types de tuyaux (qui n'ont que des dimensions différentes) */
class TuyauGraphique: public ElementGraphique { //héritage

    public:
        long double diametre;
        long double longueur;
        long double pression;
        long double temp;

        /** Créé un TuyauGraphique avec des paramètres par défaut */
        TuyauGraphique(unsigned int id);

        TuyauGraphique(long double diametre, long double longueur, long double pression, long double temp,
                       Couple pos, unsigned int rota, unsigned int id);

        /** Créé un TuyauGraphique à partir d'un objet json le décrivant */
        TuyauGraphique(const json& j);

        virtual ~TuyauGraphique();

        virtual const Couple& get_dim() const = 0;

        virtual const std::vector<Couple>& get_coo_ports() const = 0;

        virtual const std::vector<unsigned int>& get_orientation_ports() const = 0;

        virtual unsigned int get_id_type() const = 0;

        void afficher_fenetre(SDL_Renderer* renderer, SDL_Rect& rect) final;

        json get_json() const final;
};

#endif