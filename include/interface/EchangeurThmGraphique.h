#ifndef ECHANGEUR_THM_GRAPHIQUE_H
#define ECHANGEUR_THM_GRAPHIQUE_H
#include "ElementGraphique.h"
#include "../sim/Simulateur.h"


/** @brief L'échangeur thermique fait partie des éléments qui implémentent les fonctions de la classe ElementGraphique */
class EchangeurThmGraphique: public ElementGraphique { //héritage

    public:
        long double volume_espace;
        long double temperature_espace;
        bool espace_immuable;

        long double diametre;
        long double longueur;
        long double epaisseur;

        long double temperature;
        long double pression;

        Materiau materiau;

        /** Créé un EchangeurThmGraphique avec des paramètres par défaut */
        EchangeurThmGraphique(unsigned int id);

        EchangeurThmGraphique(long double volume_espace, long double temperature_espace, bool immuable, long double diametre, long double longueur, long double epaisseur, long double temperature, long double pression, Materiau materiau, Couple pos, unsigned int rota, unsigned int id);

        /** Créé un EchangeurThmGraphique à partir d'un objet json la décrivant */
        EchangeurThmGraphique(const json& j);

        ~EchangeurThmGraphique() = default;

        const Couple& get_dim() const final;

        const std::vector<Couple>& get_coo_ports() const final;

        const std::vector<unsigned int>& get_orientation_ports() const final;

        void afficher_fenetre(SDL_Renderer* renderer, SDL_Rect& rect) final;

        unsigned int get_id_type() const final;

        json get_json() const final;

    private:
        const static Couple DIMENSIONS; // la taille d'un objet ne change pas
        const static std::vector<Couple> COO_PORTS;
        const static std::vector<unsigned int> ORIENT_PORTS;
};

#endif