CC = g++
NIVEAU_OPTI = 3
CPPFLAGS=-Iexternal
FLAGS = -Wall -Iexternal -Wno-write-strings -g -O$(NIVEAU_OPTI)
OBJETS = $(shell ls src/sim/*.cpp --format=single-column | sed 's/src\/sim\/\(.*\).cpp/obj\/sim\/\1.o/')
OBJETS += $(shell ls src/interface/*.cpp --format=single-column | sed 's/src\/interface\/\(.*\).cpp/obj\/interface\/\1.o/')
OBJETS += obj/imgui.o obj/imgui_impl_sdl2.o obj/imgui_impl_sdlrenderer.o
OBJETS += obj/imgui_tables.o obj/imgui_draw.o obj/imgui_widgets.o obj/imgui_stdlib.o

HEADERS_LIBRES = include/interface/Couple.h

all: gazoduc

gazoduc: obj/main.o $(OBJETS)
	mkdir -p bin
	$(CC) $(FLAGS) $(OBJETS) obj/main.o -o bin/$@ -lSDL2 -lSDL2_image -lSDL2_ttf



obj/main.o: src/main.cpp $(HEADERS_LIBRES)
	mkdir -p obj
	$(CC) $(FLAGS) -c $< -o $@



obj/sim/%.o: src/sim/%.cpp include/sim/%.h $(HEADERS_LIBRES)
	mkdir -p obj/sim
	$(CC) $(FLAGS) -c $< -o $@

obj/interface/%.o: src/interface/%.cpp include/interface/%.h $(HEADERS_LIBRES)
	mkdir -p obj/interface
	$(CC) $(FLAGS) -c $< -o $@






obj/imgui.o: external/imgui.cpp external/imgui.h
	mkdir -p obj
	$(CC) $(FLAGS) -c $< -o $@

obj/imgui_impl_sdl2.o: external/imgui_impl_sdl2.cpp external/imgui_impl_sdl2.h
	mkdir -p obj
	$(CC) $(FLAGS) -c $< -o $@

obj/imgui_impl_sdlrenderer.o: external/imgui_impl_sdlrenderer.cpp external/imgui_impl_sdlrenderer.h
	mkdir -p obj
	$(CC) $(FLAGS) -c $< -o $@

obj/imgui_draw.o: external/imgui_draw.cpp
	mkdir -p obj
	$(CC) $(FLAGS) -c $< -o $@

obj/imgui_tables.o: external/imgui_tables.cpp
	mkdir -p obj
	$(CC) $(FLAGS) -c $< -o $@

obj/imgui_widgets.o: external/imgui_widgets.cpp
	mkdir -p obj
	$(CC) $(FLAGS) -c $< -o $@

obj/imgui_stdlib.o: external/imgui_stdlib.cpp
	mkdir -p obj
	$(CC) $(FLAGS) -c $< -o $@





doc: FORCE
	doxygen doc/Doxyfile
	open doc/html/index.html


clean:
	rm -r obj/* || true
	rm bin/* || true



FORCE:
